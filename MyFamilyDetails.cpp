
/*----------------------------------------------------------------------------------------------------------------------------------
      
	K.NAVEENA	10357	naveenakolanupaka10@gmail.com		Cell : 8374415216


       PURPOSE
       ---------
       This program is typically shows a Object oriented version of Family tree, Calcualtes the Father's
       income and savings of fathers and Son's salary compunding over certain years.

----------------------------------------------------------------------------------------------------------------------------------*/


#include<iostream>
#include<string.h>
#include<math.h>

using namespace std;

/*This person class contains data members and member functions*/  
class Person{

	protected:
	
		//declaring the data members
		string name;
		int age;
		double salary;
		double compound_interest;
		double principle,rate_of_interest=10,no_of_years=3;
		
	public:

		//default constructor for person class
		Person(){
			compound_interest=0.0;
		}

		//parameterized constructor for person class
		Person(string name,int age){
			this->name=name;
			this->age=age;
		}

		/* Method to calculate compound interest based upon the salary*/
		double cal_compound_int(double salary){
			compound_interest=((0.5*salary)*pow((1+(rate_of_interest)/100),no_of_years));
			cout<<"salary:"<<salary<<endl;
                  	cout<<"compound_interest for"<<"  "<<no_of_years<<"  years is: "<<compound_interest<<endl;
		}

                /* method used to display the person name and age*/
		void displayPersonDetails(){
			cout<<"name:"<<name<<endl;
			cout<<"age:"<<age<<endl;
		}
};

/* Father class which has inherited from Person class*/
class Father : public Person{

	private:        
		double salary;
	public:
		//default constructor for father class
                Father() {  
			salary = 0.0;
		}

		//parameterized constructor for father class
		Father(string name,int age,double salary) : Person(name,age){
			this->salary=salary;
         	}

		//method to display father details
		void displayFatherDetails(){
			Person :: displayPersonDetails();
			Person :: cal_compound_int(salary);
		}

		//destructor for father class
		~Father(){
		}
};

/* Mother class which has inherited from person class*/
class Mother : public Person{
	public:

		//parameterized constructor for mother class
		Mother(string name,int age) : Person(name,age){
		}

		//method to display mother details
		void displayMotherDetails(){
			Person :: displayPersonDetails();
		}

		//destructor for mother class
		~Mother(){
		}
};

/* Son class which has inherited from Father and Mother classes*/
class Son : public Father,public Mother{
	private:
		//declaring the local variables
		double salary,compound_interest=0.0;
		double principle,rate_of_interest=0.4,no_of_years=2;
	public:

		//parametrized constructor for son class
		Son(string name,int age,double salary) : Father(name,age,salary){
			this->salary=salary;
		}

		//method to display son details
		void displaySonDetails(){
			Father :: displayPersonDetails();
			Father :: cal_compound_int(salary);
		}

		//destructor for son class
		~Son(){
		}
};

/* Daughter class which inherits from Father and Mother*/
class Daughter : public Father,public Mother{
	private:
		//declaring the local variables
		string university;
	public:

		//parametrized constructor for daughter class
		Daughter(string name,int age,string university) : Mother(name,age){
			this->university=university;
		}

		//method to display daughter details
		void displayDaughterDetails(){
			Mother :: displayPersonDetails();
			cout<<"university:"<<university<<endl;
		}

		//destructor for daughter class
		~Daughter(){
		}
};

/* In Family class we are creating objects for the Father,Mother,Son and Daughter class*/  
class Family{
	public:

		//default constructor for family class
		Family(){	
			Father father("Mohan",48,10000.00);
			Mother mother("Umadevi",42);
			Son son("Sai teja",25,15000.00);
			Daughter daughter("Prashanthi",22,"GEETHAM UNIVERSITY");
		        cout<<endl<<"*****Father Details*****"<<endl;
			father.displayFatherDetails();
			cout<<endl<<"*****Mother Details*****"<<endl;
			mother.displayMotherDetails();
			cout<<endl<<"*****Son Details*****"<<endl;
			son.displaySonDetails();
			cout<<endl<<"*****Daughter Details*****"<<endl;
			daughter.displayDaughterDetails();
		}
};

//main method
int main(){
	
	//creating object for family class
	Family family;
}


/*================================================================================

OUTPUT:

*****Father Details*****
name:Mohan
age:48
salary:10000
compound_interest for  3  years is: 6655

*****Mother Details*****
name:Umadevi
age:42

*****Son Details*****
name:Sai teja
age:25
salary:15000
compound_interest for  3  years is: 9982.5

*****Daughter Details*****
name:Prashanthi
age:22
university:GEETHAM UNIVERSITY

*/         
