
#include<stdio.h>
#include<stdlib.h>

int searchElement(int my_array[],int low,int high,int element) {

	int mid=(low+high)/2;
	if(low>high) {
		return -1;
	}
	else {
		if(my_array[mid]>element) {
			searchElement(my_array,0,mid-1,element);
		}
		else if(my_array[mid]<element) {
			searchElement(my_array,mid+1,high,element);
		}
		else
			return mid;
	}

}

int main() {

	int my_array[10]={3,5,7,9,10,14,16,35,67,89};
	int element,index,size;
	size=sizeof(my_array)/sizeof(int);
	printf("enter the element to be search:");
	scanf("%d",&element);
	index=searchElement(my_array,0,size-1,element);
	if(index==-1) {
		printf("element %d not found\n",element);
	}
	else {
		printf("element %d found at position %d\n",element,index);
	}
	return 0;
}
