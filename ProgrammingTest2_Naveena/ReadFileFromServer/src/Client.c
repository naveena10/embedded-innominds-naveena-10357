//including the required header files
#include<Headers.h>

//defining the main function from which execution of this program will be start
int main(int argc,char* argv[]) {

	//checking whethr the command line arguments are sufficient or not
	if(argc<4) {
		printf("insufficient command line arguments\n");
		exit(1);
	}

	//declaring the local variables
	struct sockaddr_in ser_addr;
	struct hostent *server;
	int cfd,size=0;
	FILE* fp;
	char buf[256];

	//creating the socket for client
	cfd=socket(AF_INET,SOCK_STREAM,0);
	if(cfd<0) {
		printf("ERROR:socket didn't create\n");
		exit(1);
	}

	//initializing the members of sockaddr_in structure
	server=gethostbyname(argv[1]);
	ser_addr.sin_family=AF_INET;
	ser_addr.sin_port=htons(atoi(argv[2]));
	bcopy((char*)server->h_addr,(char*)&ser_addr.sin_addr.s_addr,sizeof(server->h_length));

	//connecting the client's socket to server 
	connect(cfd,(struct sockaddr *)&ser_addr,sizeof(struct sockaddr));

	//writing the file name to client's socket
	write(cfd,argv[3],strlen(argv[3]));
	sleep(2);
	//reading the file size from socket
	read(cfd,&size,sizeof(size));
	//if the file not present then printing the proper message
	if(size==0) {
		sleep(2);
		char message[50]={'\0'};
		read(cfd,message,50);
		puts(message);
		return 0;
	}
	//otherwise reading the content of the file from socket
	read(cfd,buf,sizeof(buf));
	//opening the file with given name in write mode
	fp=fopen(argv[3],"w");
	//writing the content into file
	fwrite(buf,size,1,fp);
	//closing the file descriptor	
	fclose(fp);

	//returning from main
	return 0;
}

