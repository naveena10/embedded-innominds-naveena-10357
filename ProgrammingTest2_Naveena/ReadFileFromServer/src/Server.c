//including the required header files
#include<Headers.h>

//defining the main function from which execution of this program will be start
int main(int argc,char* argv[]) {

	//checking whethr the command line arguments are sufficient or not
	if(argc<2) {
		printf("insufficient command line arguments\n");
		exit(1);
	}

	//declaring the local variables
	struct sockaddr_in ser_addr,cli_addr;
	int sfd,cfd,size=0;
	char buf[256]={'\0'};
	FILE* fp=NULL;
	socklen_t length;

	//creating the socket for server
	sfd=socket(AF_INET,SOCK_STREAM,0);
	if(sfd<0) {
		printf("ERROR:socket didn't create\n");
		exit(1);
	}
	
	//initializing the members of sockaddr_in structure
	ser_addr.sin_family=AF_INET;
	ser_addr.sin_port=htons(atoi(argv[1]));
	ser_addr.sin_addr.s_addr=INADDR_ANY;

	//binding the server socket
	if(bind(sfd,(struct sockaddr *)&ser_addr,sizeof(struct sockaddr_in))==-1) {
		printf("ERROR:binding is failed\n");
		exit(1);
	}

	//listening for clients request
	if(listen(sfd,1)<0) {
		printf("ERROR:binding is failed\n");
		exit(1);
	}

	//accepting the client's request
	length=sizeof(cli_addr);
	cfd=accept(sfd,(struct sockaddr *)&cli_addr,&length);
	if(cfd<0) {
		printf("ERROR:connect fail\n");
		exit(1);

	}

	//reading the file name
	read(cfd,buf,sizeof(buf));

	//opening the file in read mode
	fp=fopen(buf,"r");
	//if it is not present sending the proper message 
	if(fp==NULL) {
		char *message="no such file exist\n";
		write(cfd,&size,sizeof(size));
		sleep(2);
		write(cfd,message,strlen(message));
		exit(0);
	}

	//reading the size of the given file
	fseek(fp,0,2);
	size=ftell(fp);
	rewind(fp);

	//writing the size into client's socket
	write(cfd,&size,sizeof(size));
	sleep(2);
	//reading the file content into buffer
	fread(buf,size,1,fp);
	//writing the file content from buffer to client's socket
	write(cfd,buf,sizeof(buf));
	//closing the file descriptor
	fclose(fp);

	//returning from main
	return 0;
}

