//including the required header files
#include<Headers.h>

//declaring the global variables
int cfd;
char buf[256];

//defining the readFunc from which first thread execution will start
//this thread will read the data from the client and displays it on the stdout
void* readFunc(void *arg) {

	while(1) {
		read(cfd,buf,sizeof(buf));
		write(1,buf,sizeof(buf));
		memset(buf,'\0',sizeof(buf));
	}

}

//defining the readFunc from which second thread execution will start
//this thread will read the data from the stdin and write it to client 
void* writeFunc(void *arg) {
	while(1) {
		read(0,buf,sizeof(buf));
		write(cfd,buf,sizeof(buf));
		if(strcmp(buf,"bye\n")==0) {
			exit(0);
		}
		memset(buf,'\0',sizeof(buf));
	}

}

//defining the main function from which execution of this program will be start
int main(int argc,char* argv[]) {

	//checking whethr the command line arguments are sufficient or not
	if(argc<2) {
		printf("insufficient command line arguments\n");
		exit(1);
	}
	
	//declaring the local variables
	struct sockaddr_in ser_addr,cli_addr;
	int sfd;
	socklen_t length;
	pthread_t thread_id1,thread_id2;

	//creating the socket for server	
	sfd=socket(AF_INET,SOCK_STREAM,0);
	if(sfd<0) {
		printf("ERROR:socket didn't create\n");
		exit(1);
	}

	//initializing the members of sockaddr_in structure
	ser_addr.sin_family=AF_INET;
	ser_addr.sin_port=htons(atoi(argv[1]));
	ser_addr.sin_addr.s_addr=INADDR_ANY;

	//binding the server socket
	if(bind(sfd,(struct sockaddr *)&ser_addr,sizeof(struct sockaddr_in))==-1) {
		printf("ERROR:binding is failed\n");
		exit(1);
	}

	//listening for clients request
	if(listen(sfd,1)<0) {
		printf("ERROR:binding is failed\n");
		exit(1);
	}

	//accepting the client's request
	length=sizeof(cli_addr);
	cfd=accept(sfd,(struct sockaddr *)&cli_addr,&length);
	if(cfd<0) {
		printf("ERROR:connect fail\n");
		exit(1);

	}

	//creating two threads which will read and write the data from client and to client respectively
	pthread_create(&thread_id1,NULL,readFunc,NULL);
	pthread_create(&thread_id2,NULL,writeFunc,NULL);
	//joining the threads
	pthread_join(thread_id1,NULL);
	pthread_join(thread_id2,NULL);

	//returning from main
	return 0;
}

