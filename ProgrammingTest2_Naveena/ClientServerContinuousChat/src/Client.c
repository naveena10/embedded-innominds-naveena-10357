//including the required header files
#include<Headers.h>

//declaring the global variables
int cfd;
char buf[256];

//defining the readFunc from which first thread execution will start
//this thread will read the data from the server and displays it on the stdout
void* readFunc(void *arg) {

	while(1) {
		read(cfd,buf,sizeof(buf));
		write(1,buf,sizeof(buf));
		if(strcmp(buf,"bye\n")==0) {
			exit(0);
		}
		memset(buf,'\0',sizeof(buf));
	}

}

//defining the writeFunc from which second thread execution will start
//this thread will read the data from the stdin and write it to server 
void* writeFunc(void *arg) {
	while(1) {
		read(0,buf,sizeof(buf));
		write(cfd,buf,sizeof(buf));
		memset(buf,'\0',sizeof(buf));
	}

}

//defining the main function from which execution of this program will be start
int main(int argc,char* argv[]) {

	//checking whethr the command line arguments are sufficient or not
	if(argc<3) {
		printf("insufficient command line arguments\n");
		exit(1);
	}

	//declaring the local variables
	struct sockaddr_in ser_addr;
	struct hostent *server;
	pthread_t thread_id1,thread_id2;

	//creating the socket for client
	cfd=socket(AF_INET,SOCK_STREAM,0);
	if(cfd<0) {
		printf("ERROR:socket didn't create\n");
		exit(1);
	}

	//initializing the members of sockaddr_in structure
	server=gethostbyname(argv[1]);
	ser_addr.sin_family=AF_INET;
	ser_addr.sin_port=htons(atoi(argv[2]));
	bcopy((char*)server->h_addr,(char*)&ser_addr.sin_addr.s_addr,sizeof(server->h_length));

	//connecting the client's socket to server 
	connect(cfd,(struct sockaddr *)&ser_addr,sizeof(struct sockaddr));

	//creating two threads which will read and write the data from server and to server respectively
	pthread_create(&thread_id1,NULL,readFunc,NULL);
	pthread_create(&thread_id2,NULL,writeFunc,NULL);
	//joining the threads
	pthread_join(thread_id1,NULL);
	pthread_join(thread_id2,NULL);

	//returning from main
	return 0;
}

