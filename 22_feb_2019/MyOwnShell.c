/***********************************************************************************************
Author:
-------
K.NAVEENA	10357	naveenakolanupaka10@gmail.com	8374415216

Purpose:
--------
	--To create the own shell
************************************************************************************************/
//including the header files
#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include<pwd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>

//defining the macros for colours
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

//declaring the global variables
char * arguments[100];
//declaring the functions used in this file
void readCommanFromuser(void);
const char *getUserName();

//defining the main function
int main()
{
	//declaring the local variables
	int ret = -1, child_exit_status;
	char cwd[1024];
	const char *username;

	while(1)
	{
		//getting the user name from getUserName function
		username=getUserName();
		//displaying the user name 
		printf(ANSI_COLOR_GREEN "%s", username);
		printf(ANSI_COLOR_GREEN "%s", "@myOwnShell");
		printf(KWHT ":");
		getcwd(cwd, sizeof(cwd));
		char name[] = " $";
		strcat(cwd,name);
		printf( KBLU  "%s", cwd);
		printf(KWHT " ");
		readCommanFromuser();
		//if the given command is pwd then displaying the current path of the shell 
		if(!(strcmp(arguments[0],"pwd"))) {
			char cwd[1024];
			getcwd(cwd, sizeof(cwd));
			write(1, cwd, strlen(cwd));
			printf("\n");
		}
		//if the given command is exit then exiting the terminal 
		else if(!(strcmp(arguments[0],"exit"))) {
			break;
		}
		//if the given command is related to cd then performing the corresponding action
		else if(!(strcmp(arguments[0],"cd"))) {
				
			chdir(arguments[1]);
			continue;	
		}
		//else for othr commands executing the command and displaying the result
		else {
			ret = fork();
			if(ret == 0)
				execvp(arguments[0], arguments);
			else
				wait(&child_exit_status);
		}
	}
	//returning from main
	return 0;
}

//defining the getUserName function to get the user name of the current system
const char *getUserName()
{
	  uid_t uid = geteuid();
	  struct passwd *pw = getpwuid(uid);
	      if (pw)
		        {
				return pw->pw_name;
			}

	        return "";
}

//defining the readCommanFromuser function to read the commands from user
void readCommanFromuser(void) {
	char *token;
	char string[100];
	gets(string);
	int i=0;
	token=strtok(string," ");
	while(token!=NULL) {
		arguments[i] = token;
		i++;
		token=strtok(NULL," ");	
	}
	arguments[i] = token;
	return;
}
