/***********************************************************************************************
Author:
-------
K.NAVEENA	10357	naveenakolanupaka10@gmail.com	8374415216

Purpose:
--------
	--To reverse the words present in a given string

************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

//declaring th efunctions used in this file
void getString(char **string); 
void reverseWords( char*); 

int main() {
	//declaring the local variables
	char* string;
	//reading the string by calling the getString function 
	printf("enter the string:");
	getString(&string);
	//displaying the entered string before reversing the words 
	printf("\tEntered string:\n\t\t%s\n",string);
	//calling reverseWords function to reverse the words present in the given string
	reverseWords(string);
	//displaying the string after reversing the words present in the given string
	printf("\tAfter reversing the words:\n\t\t%s\n",string);
	//returning from main
	return 0;
}

//defining the getString function to read the string from stdin
void getString(char **string) {

	int size=0;
	char ch;
	char *str=NULL;
	str=(char*)malloc((size+1)*sizeof(char));
	while((ch=getchar())!='\n') {
		str=realloc(str,(size+1)*sizeof(char));
		str[size]=ch;
		size++;
	}
	str[size]='\0';
	*string=str;
	return;

}

//defing the reverseWords function which will reverse the words present in the given string
//and returns the resultant string to main
void reverseWords( char* string) {

	for(int i=0,j=strlen(string)-1;i<j;i++,j--) {
                char temp=string[i];
                string[i]=string[j];
                string[j]=temp;
        }
        for(int i=0;string[i];i++) {
                int sub_i,sub_j;
                sub_i=i;
                while(string[i]!=' ' && string[i]!='\0') {
                        sub_j=i;
                        i++;
                }
                for(;sub_i<sub_j;sub_i++,sub_j--) {
                        char temp=string[sub_i];
                        string[sub_i]=string[sub_j];
                        string[sub_j]=temp;
                }
        }
        string[strlen(string)]='\0';
	return;

}

/**********************************************************************

SAMPLE OUTPUT:
--------------
enter the string:EMBEDDED TEAM INNOMINSD HYDERABAD
	Entered string:
		EMBEDDED TEAM INNOMINSD HYDERABAD
	After reversing the words:
		HYDERABAD INNOMINSD TEAM EMBEDDED

***********************************************************************/
