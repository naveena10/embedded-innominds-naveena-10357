/***********************************************************************************************
Author:
-------
K.NAVEENA	10357	naveenakolanupaka10@gmail.com	8374415216

Purpose:
--------
	--To convert the given string to number either integer or float according to the input

************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>

//declaring the functions used in this file
float myAtoF(char*);
int myAtoI(char*);

int main() {
	//declaring the local variables
	char number[20];
	int flag=0;
	//reading the number in string format
	printf("enter the number:");
	scanf("%s",number);
	//checking whether it is float or not
	for(int i=0;i<strlen(number);i++) {
		if(number[i]=='.') {
			flag++;
		}
	}
	//if it is float calling the myAtoF function to convert it into float value
	if(flag) {

		float fnumber=myAtoF(number);
		printf("\tEntered string is float.\n\tEquivalent float number is:%f\n",fnumber);
	}
	//if it is not float calling the myAtoI function to convert it into integer value
	else {
		int inumber=myAtoI(number);
		if(inumber)
			printf("\tEntered string is integer.\n\tEquivalent int number is:%i\n",inumber);
	}

}

//defining the myAtoF function to convert the given string into float value and
//returns the result to main if it is valid otherwise returns the zero 
float myAtoF(char* string) {

	float number=0.0;
	int i,count=0,floating_flag=0,negative_flag=0;
	for(i=0;i<strlen(string);i++) {
		if(string[i]=='-' && i==0) {
			negative_flag++;
			continue;
		}
		else if(string[i]<='9' && string[i]>='0') {
			number=number*10+string[i]-48;
		}
		else if(string[i]=='.') {
			count=i;
			floating_flag++;
			if(floating_flag==1) continue;
			else break;
		}
		else break;
	}
	if(i==strlen(string)) {
		number=number/pow(10,i-count-1);
		if(negative_flag)
			return -number;
		else
			return number;
	}
	else {
		printf("\tEntered number is invalid\n");
		return 0.0;
	}

}

//defining the myAtoI function which will convert the given string to integer and
//returns the result to main if it valid otherwise returns zero
int myAtoI(char* string) {

	int i,negative_flag=0;
	int number=0;

	for(i=0;i<strlen(string);i++) {
		if(string[i]=='-' && i==0) {
			negative_flag++;
		}
		else if(string[i]>='0' && string[i]<='9') {
			number=number*10+string[i]-48;
		}
		else 
			break;
	}
	if(i==strlen(string)) {
		if(negative_flag) return -number;
		else return number;
	}
	else {
		printf("\tEntered number is invlaid\n");
		return 0;
	}

}

/****************************************************************

SAMPLE OUTPUT1:
---------------
enter the number:12345
	Entered string is integer.
	Equivalent int number is:12345

SAMPLE OUTPUT2:
---------------
enter the number:12345.6
	Entered string is float.
	Equivalent float number is:12345.599609

SAMPLE OUTPUT3:
---------------
enter the number:-12345
	Entered string is integer.
	Equivalent int number is:-12345

SAMPLE OUTPUT4:
---------------
enter the number:-123.45
	Entered string is float.
	Equivalent float number is:-123.449997

SAMPLE OUTPUT5:
---------------
enter the number:-123.45.-3
	Entered number is invalid

*****************************************************************/
