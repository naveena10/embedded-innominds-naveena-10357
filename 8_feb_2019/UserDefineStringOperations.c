/***********************************************************************************************
Author:
-------
K.NAVEENA	10357	naveenakolanupaka10@gmail.com	8374415216

Purpose:
--------
	--To perform some simple operations on strings like strcpy,strcat,strtok without using the 
	   pre-defined functions

************************************************************************************************/

#include<stdio.h>
#include<stdio_ext.h>
#include<stdlib.h>
#include<string.h>

//declarations of functions used in this file
void getString(char **string); 
void myStrcpy(char*,char*); 
char *myStrcat(char*,char*);
void myStrtok(char *str,char delimitor); 

int main() {

	//declaring the local variables
	char* string,choice;
	//reading the enteredd string by calling the getString function
	printf("enter the string:");
	getString(&string);
	//displaying the menu of some simple operations of strings 
	while(1) {
		printf("\n********SOME SIMPLE OPERATIONS ON STRINGS********\n");
		printf("\t\t1.strcpy\n\t\t2.strcat\n\t\t3.strtok\n\t\t4.Quit\n");
		//reading the user's choice
		printf("enter your choice:");
		__fpurge(stdin);
		scanf("%c",&choice);
		//navigating according to the user's choice
		switch(choice) {
			//to perform strcpy operation without using pre-defined function
			case '1':
				{
				char destination[strlen(string)];
				myStrcpy(destination,string);
				printf("\tdestination string is:%s\n",destination);
				}
				break;
			//to perform strcat operation without using pre-defined function
			case '2':
				{
				char* source;
				printf("enter another string to concatinate:");
				getString(&source);
				char* temp=myStrcat(string,source);
				printf("\tstring1:%s string2:%s\n\tAfter strcat the string is:%s\n",string,source,temp);
				free(temp);
				}
				break;
			//to perform strtok operation without using the pre-defined function
			case '3':
				{
				char delimitor;
				printf("enter the delimitor(by which char do you want to separate the string):");
				__fpurge(stdin);
				delimitor=getchar();
				myStrtok(string,delimitor);
				}
				break;
			//to exit from main
			case '4':
				printf("\texiting....\n");
				exit(0);
			//for invalid choice
			default:
				printf("\tinvalid choice.\n");
		}
	}
	
	return 0;
}

//defining the getString function which will read the string from stdin
void getString(char **string) {

	int size=0;
	char ch;
	char *str=NULL;
	str=(char*)malloc((size+1)*sizeof(char));
	__fpurge(stdin);
	while((ch=getchar())!='\n') {
		str=realloc(str,(size+1)*sizeof(char));
		str[size]=ch;
		size++;
	}
	str[size]='\0';
	*string=str;
	return;

}

//defining myStrcpy function which will copy the given source string to 
//the given destination string
void myStrcpy(char *destination,char* source) {
	for(int i=0;i<strlen(source);i++) {
		destination[i]=source[i];
	}
	destination[strlen(source)]='\0';
	return;
}

//defining the myStrcat function which will concatinate the given second string to the 
//given first string and returns the resultant string to main
char *myStrcat(char* destination,char* source) {
	char *temp;
	temp=(char*)malloc((strlen(source)+strlen(destination)+1)*sizeof(char));
	int i;
	for(i=0;i<strlen(destination);i++) {
			temp[i]=destination[i];
	}
	int j=i;
	for(i=0;i<strlen(source);i++,j++) {
		temp[j]=source[i];
	}
	temp[strlen(temp)]='\0';
	return temp;
}

//defining the myStrtok function which will separate the given string by the given delimitor
void myStrtok(char *str,char delimitor) {
	char *token=(char *)calloc(1,strlen(str)*sizeof(char));
	char str1[2]={delimitor,'\0'};
	int i=0,j=0;
	for(i=0;i<strlen(str);i=j+1) {
		if(token!=NULL) j=i;
		while(str[j]!=str1[0]) {
			if(str[j]=='\0') break;
			j++;
		}
		strncpy(token,str+i,j-i);
		token[j-i]='\0';
		printf("\t\t%s\n",token);
	}
	return;
}

/**************************************************************************
SAMPLE OUTPUT:
--------------
enter the string:EMBEDDED TEAM

********SOME SIMPLE OPERATIONS ON STRINGS********
		1.strcpy
		2.strcat
		3.strtok
		4.Quit
enter your choice:1
	destination string is:EMBEDDED TEAM

********SOME SIMPLE OPERATIONS ON STRINGS********
		1.strcpy
		2.strcat
		3.strtok
		4.Quit
enter your choice:2
enter another string to concatinate: INNOMINDS
	string1:EMBEDDED TEAM string2: INNOMINDS
	After strcat the string is:EMBEDDED TEAM INNOMINDS

********SOME SIMPLE OPERATIONS ON STRINGS********
		1.strcpy
		2.strcat
		3.strtok
		4.Quit
enter your choice:3
enter the delimitor(by which char do you want to separate the string):
		EMBEDDED
		TEAM

********SOME SIMPLE OPERATIONS ON STRINGS********
		1.strcpy
		2.strcat
		3.strtok
		4.Quit
enter your choice:4
	exiting....

*************************************************************************/
