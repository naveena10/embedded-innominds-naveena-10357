/***********************************************************************************************
Author:
-------
K.NAVEENA	10357	naveenakolanupaka10@gmail.com	8374415216

Purpose:
--------
	--To remove the repeated the characters present in the given string

************************************************************************************************/

#include<string.h>
#include<stdio.h>
#include<stdlib.h>

//declaring the functions used in this file
void getString(char**);
char* removeRepeatedCharacters(char*); 

int main()
{
	//declaring the local variable string
	char *string;

	//reading the input by calling the getString function
	printf("enter the data:");
	getString(&string);

	//displaying the string before removing the repeated characters
	printf("\n\tthe entered string is:\n");
	printf("\t\t%s\n",string);
	
	//calling the removeRepeatedCharacters function 
	//to remove the repeated characters
	string=removeRepeatedCharacters(string);
	
	//displaying the string after removing the repeated characters
	printf("\tafter removing the repeated characters:\n");
	printf("\t\t%s\n",string);
}

//defining the getString function to read the string from stdin
void getString(char **string) {

	int size=0;
	char ch;
	char *str=NULL;
	str=(char*)malloc((size+1)*sizeof(char));
	while((ch=getchar())!='\n') {
		str=realloc(str,(size+1)*sizeof(char));
		str[size]=ch;
		size++;
	}
	str[size]='\0';
	*string=str;
	return;

}

/*defining the removeRepeatedCharacters function
to remove the repeated characters in the given string*/
char* removeRepeatedCharacters(char *str) {
	for(int i=0;str[i];i++)
		for(int j=i+1;str[j];j++)
			if(str[i]==str[j])
			{
				memmove(str+j,str+j+1,strlen(str+j+1)+1);
				j--;
			}
	return str;

}

/********************************************************
 
SAMPLE OUTPUT:
--------------
enter the data:silence is the source of strength

	the entered string is:
		silence is the source of strength
	after removing the repeated characters:
		silenc thourfg
********************************************************/
