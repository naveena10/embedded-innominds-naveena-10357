/*****************************************************************************************
Author:
-------
K.NAVEENA	10357	naveenakolanupaka10@gmail.com	8374415216

Purpose:
--------
	--To reverse the given float number.

******************************************************************************************/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>

//declarations of functions used in this file
void getString(char **string); 
float reverseFloat(char *string); 

int main() {

	//declaring the local variables
	char* string;
	float float_number=0.0;
	//reading the float value by calling getString function
	printf("enter a float value:");
	getString(&string);
	//displaying the entered float value
	printf("\tEntered float value:%s\n",string);
	//calling the reverseFloat function to reverse the given string
	float_number=reverseFloat(string);
	//if it is valid,displaying the resultant float value
	if(f)
		printf("\tAfter reversing the equivalent value:%f\n",f);
	//returning from main 
	return 0;
}

//getString function which will read the string from stdin 
void getString(char **string) {
	int size=0;
	char ch;
	char *str=NULL;
	str=(char*)malloc((size+1)*sizeof(char));
	while((ch=getchar())!='\n' && ch!=' ') {
		str=realloc(str,(size+1)*sizeof(char));
		str[size]=ch;
		size++;
	}
	str[size]='\0';
	*string=str;
}

//reversefloat function which will reverse the given float string and 
//returns the result to main if it is valid float number
//otherwise returns zero 
float reverseFloat(char *string) {
	int count=0,number=0;

	for(int i=0,j=strlen(string)-1;i<j;i++,j--) {
		if(string[i]=='-'&& i==0) 
			i=i+1;
		char temp=string[i];
		string[i]=string[j];
		string[j]=temp;
	}

	for(int i=strlen(string)-1;i>=0;i--) {
                if(string[i]!='.')
                        count++;
                else
                        break;
        }

	for(int i=0;i<strlen(string);i++) {
		if( (string[i]=='-' && i==0) || (string[i]=='.' && i==strlen(string)-count-1) ) 
			continue;
                if(string[i]>='0' && string[i]<='9')
                        number=(number*10)+(string[i]-48);
		else {
			printf("\ttered string is invalid\n");
			return 0.0;
		}
        }

	if(string[0]=='-')
		return -(number/pow(10,count));
	else
		return number/pow(10,count);

}

/***************************************************************

OUTPUT1:
-------
enter a float value:1234.567
	Entered float value:1234.567
	After reversing the equivalent value:765.432129

OUTPUT2:
--------
enter a float value:-123.45
	Entered float value:-123.45
	After reversing the equivalent value:-54.320999

OUTPUT3:
-------
enter a float value:-123.45.67
	Entered float value:-123.45.67
	entered string is invalid

***************************************************************/
