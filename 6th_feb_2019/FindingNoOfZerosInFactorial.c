/*****************************************************************************************
Author:
-------
K.NAVEENA	10357	naveenakolanupaka10@gmail.com	8374415216

Purpose:
--------
	--To find-out the number of zeros present at last in the given number's factorial

******************************************************************************************/

#include<stdio.h>
#include<math.h>

//findNoOfZeros function to find the number of zeros present at last
//int the given number's factorial
int findNoOfZeros(int data) {
	int no_of_zeros=0;
	int count=1;
	while(data >= pow(5,count)) {
		no_of_zeros+=(data/pow(5,count));
		count++;
	}
	return no_of_zeros;
}

//main function which calls the findNoOfZeros function
int main() {

	//declaring the local variables
	int data,no_of_zeros;
	
	//reading the data from user
	printf("enter the data:");
	scanf("%d",&data);
	
	//re-reading the data if it is invalid
	while(data<=0) {
		printf("****Invalid data****\n");
		printf("Data cannot be less than 0.Enter the valid data:");
		scanf("%d",&data);
	}

	//calling the findNoOfZeros function to check how many
	//zeros present in the given number's factorial
	no_of_zeros=findNoOfZeros(data);
	
	//displaying the result
	printf("number of zeros in the factorial of given data:%d\n",no_of_zeros);
	
	//returning from main
	return 0;

}

/***************************************************

OUTPUT:
-------
enter the data:100
number of zeros in the factorial of given data:24

****************************************************/
