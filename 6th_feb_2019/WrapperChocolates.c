/*****************************************************************************************
Author:
-------
K.NAVEENA	10357	naveenakolanupaka10@gmail.com	8374415216

Purpose:
--------
	--To find-out how many maximum chocolates we will get with a cash-back of 30% on 
	  each wrapper

******************************************************************************************/

#include<stdio.h>

int main() {
	
	//initializing the chocolate cost and cash-bash percentage on wrapper
	int chocolate_cost=10,cash_back_in_percentage=30;
	float cash_back_in_rupees=3;
	//declaring the local variables
	int amount,no_of_chocolates,return_amount,remaining_amount,extra_chocolates,total_amount,max_chocolates;
	//reading the amount from user
	printf("how much money do you have?\n");
	scanf("%d",&amount);
	//calculating the maximum no.of choacolates for given amount including the cash-back amount
	no_of_chocolates=amount/chocolate_cost;
	remaining_amount=amount%chocolate_cost;
	total_amount=remaining_amount+(no_of_chocolates*cash_back_in_rupees);
	extra_chocolates=total_amount/chocolate_cost;
	return_amount=total_amount%chocolate_cost;
	max_chocolates=no_of_chocolates+extra_chocolates;
	//displaying the result
	printf("1chocolate=%d/-\n",chocolate_cost);
	printf("For %d/- you will get maximum %d chocolates including cash back of %d percentage and %d/-\n",amount,max_chocolates,cash_back_in_percentage,return_amount);
	//returning from main
	return 0;
}

/****************************************************************************************

SAMPLE OUTPUT:
--------------

how much money do you have?
110
1chocolate=10/-
For 110/- you will get maximum 14 chocolates including cash back of 30 percentage and 3/-

*****************************************************************************************/
