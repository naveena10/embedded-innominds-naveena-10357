/****************************************************************
Author:
-------
K.NAVEENA 10357 naveenakolanupaka10@gmail.com 8374415216

Purpose:
--------
	--To check whether the given number is palindrome or not
 
****************************************************************/

#include <stdio.h>
#include<math.h>

//countNoOfDigits function to count the number of digits 
//present in the given number 
int countNoOfDigits(int data) {
	int no_of_digits=0,temp=data;
	while(temp%10 || temp) {
		no_of_digits++;
		temp/=10;
	}
	return no_of_digits;
}

//isPalindrome function to check whether the 
//given number is palindrome or not
int isPalindrome(int data) {

	//declaring the local variables
	int temp=data,no_of_digits;
	int first_term,second_term;
	int first_element,last_element;
	
	//calling the countNoOfDigits function 
	//to get the number of digits present the given number
	no_of_digits=countNoOfDigits(data);
	
	//getting fist and last elements in the given number
	first_element=temp/pow(10,no_of_digits-1);
	last_element=temp%10;

	//comparing the elements if equal then going for next comparision
	while( first_element == last_element ) {
		first_term=pow(10,no_of_digits-1);
		second_term=temp%first_term;
		temp= second_term/10;
		no_of_digits-=2;
		if(no_of_digits == 1 || no_of_digits==0) {
			return 1;
		}
		first_element=temp%10;
		last_element=temp/pow(10,no_of_digits-1);
	}
	
	//returning to main
	return 0;
}

//main function to call all other functions
int main()
{
	//declaring the local variables
	int data,flag=0;
	//reading the data
	printf("enter data:");
	scanf("%d",&data);
	//calling the isPalindrome function 
	flag=isPalindrome(data);
	//displaying the corresponding message
	if(flag) {
		printf("entered data is palindrome\n");
	} else {
		printf("entered data is not palindrome\n");
	}
	//returning from main
	return 0;
}

/*********************************************************
OUTPUT1:
--------
enter data:123404321
entered data is palindrome

OUTPUT2:
--------
enter data:12341234
entered data is not palindrome
*********************************************************/
