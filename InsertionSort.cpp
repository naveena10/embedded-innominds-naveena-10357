/****************************************************************************************************** 

  Author          
  -------
  K.NAVEENA      10357   naveenakolanupaka10@gmail.com     Cell : 8374415216

  Purpose:
  --------
	--To sort the given array elents by using Insertion Sort technique.

*********************************************************************************************************/

#include<iostream>

using namespace std;

//creating the InsertionSort class
class InsertionSort {
	//declaring the data members
	int *myarray;
	int size;
	//defining the member functions as public
	public:
	InsertionSort(int size);
	void insertionSort();	
	void printArray();
};

//parametrized constructor for InsertionSort class
InsertionSort::InsertionSort(int size) {
	this->size=size;
	myarray= new int[size];
	cout<<"enter array elements:"<<endl;
	for(int i=0;i<size;i++)
		cin>>myarray[i];
}

//member function to insertionSort to sort the array elements
void InsertionSort::insertionSort() {
	int i,j,temp;
	for(int i=1;i<size;i++)
	{
		int j=i-1;
		temp=myarray[i];
		while(j>=0 && myarray[j]>temp) {
			myarray[j+1]=myarray[j];
			j--;
		}
		myarray[j+1]=temp;
	}

}

//member function printArray to print the array elements 
void InsertionSort::printArray() {
	for(int i=0;i<size;i++)
		cout<<myarray[i]<<" ";
	cout<<endl;
}


int main()
{
	//declaing the local variables
	int size;
	//reading the array size
	cout<<"enter the array size:"<<endl;
	cin>>size;
	// creating object for InsertionSort class
	InsertionSort object(size);
	cout<<"array elements before sorting:"<<endl;
	// calling print array method
	object.printArray();
	// calling insertionSort method
	object.insertionSort();
	cout<<"array elements after sorting:"<<endl;
	// calling print array method
	object.printArray();
	return 0;
}

/*****************************************************
OUTPUT:
-------
enter the array size:
5
enter array elements:
3
9
2
6
1
array elements before sorting:
3 9 2 6 1 
array elements after sorting:
1 2 3 6 9 
*******************************************************/
