//including the required header files
#include<Headers.h>

//declaring the global variables
int cfd;
char buf[256]={'\0'};

//defining the readFromServer function from which frist thread execution will be start
void* readFromServer(void* arg) {
	
	while(1) {
		read(cfd,buf,255);
		printf("\t%s\n",buf);
		memset(buf,'\0',sizeof(buf));
	}
}

//defining the writeToServer function from which second thread execution will be start
void* writeToServer(void *arg) {
	
	while(1) {
		read(0,buf,sizeof(buf));
		write(cfd,buf,strlen(buf));
		memset(buf,'\0',sizeof(buf));
	}
}

//defining the main function from which execution will be start
int main(int argc,char* argv[]) {

	//checking whether the command line arguments are sufficient or not
	if(argc<3) {
		perror("insufficient command line arguments\n");
		exit(1);
	}

	//declaring the local variables
	struct sockaddr_in ser_addr;
	struct hostent *server;
	pthread_t id1=0,id2=0;

	//creating the client socket through socket system call
	cfd=socket(AF_INET,SOCK_STREAM,0);
	//displaying the error message if it fails
	if(cfd<0) {
		printf("ERROR:failed in opening the socket\n");
		exit(1);
	}

	//getting the server details through gethostbyname system call
	server=gethostbyname(argv[1]);

	//initializing the members of sockaddr_in structure
	ser_addr.sin_family=AF_INET;
	ser_addr.sin_port=htons(atoi(argv[2]));
	ser_addr.sin_addr.s_addr=INADDR_ANY;

	bcopy((char*)server->h_addr,(char*)&ser_addr.sin_addr.s_addr,server->h_length);

	//connecting the client socket to server through connect system call
	connect(cfd,(struct sockaddr *)&ser_addr,sizeof(ser_addr));

	pthread_create(&id1,NULL,readFromServer,NULL);
	pthread_create(&id1,NULL,writeToServer,NULL);
	pthread_join(id1,NULL);
	pthread_join(id2,NULL);

	//returning from main
	return 0;

}
