//including the required header files
#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<pthread.h>

//defining the function from which thread execution will be start
void *func() {

	printf("inside thread before sleep\n");
	sleep(1);
	printf("inside thread after sleep\n");
	return NULL; 
}

//main function from which execution will be start
int main() {
	
	//declaring the local variables
	pthread_t thread1,thread2,thread3;
	
	printf("before thread creation\n");

	//creating the threads using pthread_create
	pthread_create(&thread1,NULL,func,NULL);
	pthread_create(&thread2,NULL,func,NULL);
	pthread_create(&thread3,NULL,func,NULL);
	
	//joining the threads
	pthread_join(thread1,NULL);
	pthread_join(thread2,NULL);
	pthread_join(thread3,NULL);
	
	printf("after thread execution\n");

	//returning from main
	return 0;
}

/****************************************************************
SAMPLE OUTPUT:
--------------
before thread creation
inside thread before sleep
inside thread before sleep
inside thread before sleep
inside thread after sleep
inside thread after sleep
inside thread after sleep
after thread execution
*****************************************************************/
