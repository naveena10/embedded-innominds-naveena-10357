//including the required header files
#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<pthread.h>

//declaring the structure
typedef struct arguments {
       int a;
	char b;
}arg;	

//defining the function from which thread execution will be start
void *func(void *pointer) {

	arg *ar=(arg *)pointer;
	printf("before incrementing:");
	printf("a=%d b=%c\n",ar->a,ar->b);
	ar->a++;
	ar->b++;
	sleep(1);
	printf("after incrementing:");
	printf("a=%d b=%c\n",ar->a,ar->b);
	return NULL; 

}

//defining the main from which the execution will be start
int main() {
	
	//declaring the local variables
	pthread_t thread1,thread2,thread3;

	//declaring pointer for structure arguments 
	arg *pointer=(arg*)malloc(sizeof(arg));

	//assigning the values for structure members	
	pointer->a=10;
	pointer->b='a';
	
	//creating the threads using pthread_create
	pthread_create(&thread1,NULL,func,pointer);
	pthread_create(&thread2,NULL,func,pointer);
	pthread_create(&thread3,NULL,func,pointer);
	
	//joining the threads using pthread_join
	pthread_join(thread1,NULL);
	pthread_join(thread2,NULL);
	pthread_join(thread3,NULL);

	//returning from main	
	return 0;
}

/*******************************************************************
SAMPLE OUTPUT:
--------------
before incrementing:a=10 b=a
before incrementing:a=11 b=b
before incrementing:a=12 b=c
after incrementing:a=13 b=d
after incrementing:a=13 b=d
after incrementing:a=13 b=d
********************************************************************/
