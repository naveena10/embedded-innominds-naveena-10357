//declaring the required header files
#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<pthread.h>

//declaring the structure arguments
typedef struct arguments {
	int arg1;
	char arg2;
}arg;

//declaring the global variables
pthread_mutex_t lock; 	

//defining the function from which thread execution will be start
void *func(void *pointer) {

	pthread_mutex_lock(&lock); 
	arg *args=(arg *)pointer;
	printf("before incrementing:");
	printf("arg1=%d arg2=%c\n",args->arg1,args->arg2);
	args->arg1++;
	args->arg2++;
	sleep(1);
	printf("after incrementing:");
	printf("arg1=%d arg2=%c\n",args->arg1,args->arg2);
	pthread_mutex_unlock(&lock); 
	return NULL; 

}

//defining the main function from which execution will be start
int main() {
	
	//declaring the local variables
	pthread_t thread1,thread2,thread3;

	//declaring the pointer for structure
	arg *pointer=(arg*)malloc(sizeof(arg));
	
	//assigning the values to structure members
	pointer->arg1=10;
	pointer->arg2='a';
	
	//initializing the mutex variable 
	if (pthread_mutex_init(&lock, NULL) != 0) 
    	{ 
        	printf("\n mutex init has failed\n"); 
        	return 1; 
    	} 

	//creating the threads using pthread_create
	pthread_create(&thread1,NULL,func,pointer);
	pthread_create(&thread2,NULL,func,pointer);
	pthread_create(&thread3,NULL,func,pointer);
	
	//joining hte threads using pthread_join
	pthread_join(thread1,NULL);
	pthread_join(thread2,NULL);
	pthread_join(thread3,NULL);

	//destroying the mutex variable
	pthread_mutex_destroy(&lock);

	//returning from main	
	return 0;
}
/*************************************************************
SAMPLE OUTPUT:
--------------
before incrementing:arg1=10 arg2=a
after incrementing:arg1=11 arg2=b
before incrementing:arg1=11 arg2=b
after incrementing:arg1=12 arg2=c
before incrementing:arg1=12 arg2=c
after incrementing:arg1=13 arg2=d
*************************************************************/

