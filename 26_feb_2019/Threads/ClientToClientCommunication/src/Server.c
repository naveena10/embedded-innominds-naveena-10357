//including the required header files
#include<Headers.h>

//declaring the global variables
int cfd1,cfd2;
char buf[256]={'\0'};

//defining readWrite function from which first thread execution will be start 
//this thread will read the data from first client and write into the second client's socket
void *readWrite(void *b) {

	while(1) {
		read(cfd1,buf,255);
		write(cfd2,buf,strlen(buf));
		memset(buf,'\0',sizeof(buf));
	}


}

//defining writeRead function from which second thread execution will be start 
//this thread will read the data from second client and write into the first client's socket
void *writeRead(void *b) {

	while(1) {
		read(cfd2,buf,255);
		write(cfd1,buf,strlen(buf));
		memset(buf,'\0',sizeof(buf));

	}

}

int main(int argc,char* argv[]) {

	//checking whether the command line arguments are sufficient or not
	if(argc<2) {
		perror("insufficient command line arguments\n");
		exit(1);
	}

	//declaring the local variables
	int sfd;

	struct sockaddr_in ser_addr,cli_addr;
	socklen_t len;
	pthread_t id1,id2;

	//creating the server socket
	sfd=socket(AF_INET,SOCK_STREAM,0);

	//if it fails printing the error message
	if(sfd<0) {
		printf("ERROR:failed in opening the socket\n");
		exit(1);
	}

	//initializing the variables of structure sockaddr_in 
	ser_addr.sin_family=AF_INET;
	ser_addr.sin_port=htons(atoi(argv[1]));
	ser_addr.sin_addr.s_addr=INADDR_ANY;

	//binding the server socket if it fails printing the error message
	if(bind(sfd,(struct sockaddr *)&ser_addr,sizeof(ser_addr))==-1) {
		perror("ERROR:binding failed\n");
		exit(1);
	}

	//listen to client requests through listen call if it fails printing the error message
	if(listen(sfd,1)<0) {
		printf("ERROR:Listen call fails\n");
		exit(0);
	}

	//accepting the client's request through accept call
	len=sizeof(cli_addr);
	cfd1=accept(sfd,(struct sockaddr *)&cli_addr,&len);
	//if it fails displaying the error message
	if(cfd1<0) {
		perror("ERROR:accept failed\n");
		exit(1);
	}	
	//if it fails displaying the error message
	cfd2=accept(sfd,(struct sockaddr *)&cli_addr,&len);
	if(cfd2<0) {
		perror("ERROR:accept failed\n");
		exit(1);
	}	

	//creating the threads using pthread_create
	pthread_create(&id1,NULL,readWrite,NULL);
	pthread_create(&id2,NULL,writeRead,NULL);

	//joining the threads using pthread_join
	pthread_join(id1,NULL);
	pthread_join(id2,NULL);

	//returning from main
	return 0;

}
