/***********************************************************************************************
Author:
-------
K.NAVEENA	10357	naveenakolanupaka10@gmail.com	8374415216

Purpose:
--------
	--To implement the ls command
************************************************************************************************/

#include<stdio.h>
#include<dirent.h>
#include<stdlib.h>
#include<string.h>

int main(int argc,char* argv[])
{
	//declaring the local variables
	DIR *dp;
	struct dirent *ptr;
	
	//checking whether the path is provided or not
	//if not opening the current directory
	if(argc==1) {
		dp=opendir("./");
	} 
	//else opening the given directory
	else { 
		dp=opendir(argv[1]);
	}
	
	//checking whether the given path is exist or not
	//if not printing the corresponding message
	if(dp==NULL) {
		printf("ls: cannot access 'desktop': No such file or directory\n");
		exit(0);
	}

	//otherwise printing the list of the files present in that directory
	while(ptr=readdir(dp))
		if(ptr->d_name[0]!='.')
			printf("%s   ",ptr->d_name);
	printf("\n");

	//returning from main
	return 0;
}

/*********************************************************

SAMPLE OUTPUT1:
---------------
./a.out desktop
ls: cannot access 'desktop': No such file or directory

SAMPLE OUTPUT2:
---------------
./a.out 
PwdCommand.c   LsCommand.c   a.out   ExtractIP.sh   

SAMPLE OUTPUT3:
---------------
./a.out ~/Desktop/
Subhasis   navna   
**********************************************************/
