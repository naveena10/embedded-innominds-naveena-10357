/****************************************************************************************************** 
  	Author			
        -------
	k.NAVEENA	10357	naveenakolanupaka10@gmail.com	Cell : 8374415216

        Purpose:
        --------

        --To store the details of an employee in the given csv file

******************************************************************************************************/
//including the header files
#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>

//declaring the structure Employee with name and id as data members
struct Employee {
	int id;
	char *name;
};

//declaring the getName function
void getName(char **name); 

//defining the main function 
int main(int argc,char* argv[]) {

	//checking whether the command line arguments are sufficient or not
	//if not printing the error message
	if(argc<2) {
		printf("insufficient command line arguments\n");
		exit(1);
	}

	//declaring the local variables
	FILE *fp=NULL;
	int count;
	
	//reading the no.of employees count
 	printf("enter the how many no.of employees details you are entering :");
	scanf("%d",&count);

	//allocating the memory for given no.of records
	struct Employee *temp=(struct Employee*)calloc(sizeof(struct Employee)*count,1);
	//if fails printing the error message
	if(temp == NULL) {
		printf("Failed in allocating memory\n");
		exit(1);
	}

	//opening the Employee.csv file in appending mode to append the new entered data
	//if it fails priting the error message
	if((fp = fopen(argv[1],"a+")) == NULL) {
		printf("Failed to open the file\n");
		exit(1);
	}
	else
		printf("FILE OPENED SUCCESSFULLY\n");

	//writing the data onto file
	while(count!=0) {
		printf("Enter student id :\n");
		scanf("%d",&temp->id);
		getchar();
		fprintf(fp,"%d",temp->id);
		printf("Enter student  name :\n");
		getName(&temp->name);
		fputs(",",fp);
		fputs(temp->name,fp);
		fputs("\n",fp);
		count--;
	}
	
	//freeing the temp
	free(temp);

	//closing the file descriptor
	fclose(fp);

	//returning from main
	return 0;
}

//defining the getName method to get the name from the user
void getName(char **name) {

	int size=0;
	char ch;
	char *str=NULL;
	str=(char*)malloc((size+1)*sizeof(char));
	while((ch=getchar())!='\n') {
		str=realloc(str,(size+1)*sizeof(char));
		str[size]=ch;
		size++;
	}
	str[size]='\0';
	*name=str;
	return;

}

/*****************************************************
SAMPLE OUTPUT:
---------------
	enter the how many no.of employees details you are entering :2
	FILE OPENED SUCCESSFULLY
	Enter student id :
	6
	Enter student  name :
	nandini
	Enter student id :
	7
	Enter student  name :
	rama

----->cat Employee.csv 
	1,navna
	2,naidu
	3,bhaskar
	4,vidya
	5,javed
	6,tataji
	6,nandini
	7,rama
**************************************************************/

