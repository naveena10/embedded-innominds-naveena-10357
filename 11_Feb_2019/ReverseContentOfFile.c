/****************************************************************************************************** 
  	Author			
        -------
	k.NAVEENA	10357	naveenakolanupaka10@gmail.com	Cell : 8374415216

        Purpose:
        --------

        --To reverse the content of the given file and storing the resultant content into another file

******************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>

int main(int argc,char *argv[]) {

	//declaring the local variables
	FILE* fp;
	char *buf;
	int size;
	//checking whether the command line arguments are sufficient or not
	if(argc<3) {
		printf("insufficient comman line arguments\n");
		exit(0);
	}
	//opening the source file in read mode if it fails printing the error message
	fp=fopen(argv[1],"r");
	if(fp==NULL) {
		printf("can't open file:%s\n",argv[1]);
		exit(1);
	}
	//calculating the size of the file
	fseek(fp,0,2);
	size=ftell(fp);
	rewind(fp);
	//allocating the memory for buffer
	buf=(char*)calloc(1,(size+1)*sizeof(char));
	//reading the file content into buffer
	fread(buf,size,1,fp);
	//reversing the content present in the buffer
	for(int i=0,j=size-1;i<j;i++,j--) {
		buf[j]^=buf[i]^=buf[j]^=buf[i];
	}
	//closing the source file descriptor
	fclose(fp);
	//opening the destination file in write mode if fails printing the error message
	fp=fopen(argv[2],"w");
	if(fp==NULL) {
		printf("can't open file:%s\n",argv[2]);
		exit(1);
	}
	//writing the buffer content onto destination file
	fwrite(buf,size,1,fp);	
	//closing the destination file descriptor
	fclose(fp);
	//returning from main
	return 0;
}

/*********************************************************
SAMPLE OUTPUT:
--------------
./a.out a.txt b.txt

--->cat a.txt
	hello innominds team
	this is embedded group
	saying you to hai.

---->cat b.txt

	.iah ot uoy gniyas
	puorg deddebme si siht
	maet sdnimonni olleh

*********************************************************/
