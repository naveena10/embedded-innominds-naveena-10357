/***********************************************************************************************
Author:
-------
K.NAVEENA	10357	naveenakolanupaka10@gmail.com	8374415216

Purpose:
--------
	--To merge the given two source files' content into the given destination file 

************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

//declaring the functions used in the file
char* readFromFile(FILE*);
void writeToFile(FILE*,char*);

int main(int argc,char *argv[]) {

	//checking whether the given command line arguments are sufficient or not
	if(argc<4) {
		printf("insufficient command line arguments\n");
		exit(1);
	}

	//declaring the local variables
	FILE *sfp,*dfp;
	char *buf;

	//opening the first source file
	sfp=fopen(argv[1],"r");
	//if not opened printing the error message
	if(sfp==NULL) {
		printf("can't open file:%s\n",argv[1]);
		exit(1);
	}

	//by calling readFromFile function reading 
	//the content of the file into buffer if it is successfully opened
	buf=readFromFile(sfp);
	fclose(sfp);

	//opening the destination file
	dfp=fopen(argv[3],"w");
	//if not opened printing the error message
	if(dfp==NULL) {
		printf("can't open file:%s\n",argv[3]);
		exit(0);
	}

	//by calling writeToFile function writing the content of source file 
	//into destination if it is successfully opened
	writeToFile(dfp,buf);
	fclose(dfp);

	//opening the second source file	
	sfp=fopen(argv[2],"r");
	//if not opened printing the error message
        if(sfp==NULL) {
                printf("can't open file:%s\n",argv[2]);
                exit(1);
        }

	//by calling readFromFile function reading 
	//the content of the file into buffer if it is successfully opened
        buf=readFromFile(sfp);
	fclose(sfp);
	
	//opening the destination file
        dfp=fopen(argv[3],"a");
	//if not opened printing the error message
        if(dfp==NULL) {
                printf("can't open file:%s\n",argv[3]);
                exit(0);
        }

	//by calling writeToFile function writing the content of source file 
	//into destination if it is successfully opened
        writeToFile(dfp,buf);
	fclose(dfp);

	return 0;
}

//defining the readFromFile function to read the content of the given file
char* readFromFile(FILE *fp) {
	char *buf;
	int size;
	fseek(fp,0,2);
        size=ftell(fp);
        rewind(fp);
        buf=(char*)calloc(1,size*sizeof(char));
        fread(buf,size,1,fp);
	return buf;
}

//defining the writeToFile function to write the given content into given file
void writeToFile(FILE* fp,char *buf) {
        fwrite(buf,strlen(buf),1,fp);
	return;
}

/********************************************************

SAMPLE OUTPUT:
--------------
./a.out a.txt b.txt c.txt

----->cat a.txt
Hello Innominds
This is Naveena
Now under training under Embedded team

----->cat b.txt
Hello Naveena
Glad to hear you.
We wish you all the best for your training program.
From innominds.

-----> cat c.txt
Hello Innominds
This is Naveena
Now under training under Embedded team
Hello Naveena
Glad to hear you.
We wish you all the best for your training program.
From innominds.
********************************************************/
