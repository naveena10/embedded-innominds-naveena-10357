/************************************************************************************************* 
  	Author			
        -------
	k.NAVEENA	10357	naveenakolanupaka10@gmail.com	Cell : 8374415216

        Purpose:
        --------

        --To implement the wc command
	   Finding the given file size

**************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>

int main(int argc,char* argv[]) {

	//checking whether the command line arguments are sufficient or not
	//if not printing the error message
	if(argc<2) {
		printf("insufficient command line arguments\n");
		exit(1);
	}

	//declaring the local variables
	FILE* fp;
	char *buf;
	int size=0;
	int word_count=0,line_count=0;

	//opening the given file through fopen system call	
	fp=fopen(argv[1],"r");
	//if it fails printing the error message
	if(fp==NULL) {
		printf("can't open file:%s\n",argv[1]);
		exit(0);
	}

	//calculating the size
	fseek(fp,0,2);
	size=ftell(fp);
	rewind(fp);

	//reading the file content into buffer
	buf=(char*)calloc(1,(size+1)*sizeof(char));
	fread(buf,1,size,fp);

	//calculating the word count and line count
	for(int i=0;i<size;i++) {
		if(buf[i]==' ' || (buf[i]==10 && buf[i+1]!=10) ) 
			word_count++;
		if(buf[i]=='\n') line_count++;
	}

	//displaying the result
	printf("%d %d %d %s\n",line_count,word_count,size,argv[1]);

	//closing the file descriptor
	fclose(fp);

	//returning from main
	return 0;
}

/*****************************************************
SAMPLE OUTPUT:
--------------
	./a.out a.txt
	 3 11 63 a.txt

	using wc--->wc a.txt
	 3 11 63 a.txt
*****************************************************/
