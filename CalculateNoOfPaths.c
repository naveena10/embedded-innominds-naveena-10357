/****************************************************************************************************** 
        Author                  
        -------
        k.NAVEENA       10357   naveenakolanupaka10@gmail.com   Cell : 8374415216

        Purpose:
        --------

        --To find out the number of possible paths to travel from first element to last element
          in a 2d-array
	--Here we are taking the no.of rows and columns as run-time input so that user can choose
	  the size of the 2d-array

******************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>

//declaring the functions used in the file
int **getArray(int,int);
void printArray(int **,int,int);

//main function to call the all other functions
int main() {

	//declaring the local variables
	int rows,columns,**my_array;

	//reading the no.of rows and columns of the array
	printf("enter the no.of rows and columns:");
	scanf("%d%d",&rows,&columns);

	//calling getArray function to generate the array
	my_array=getArray(rows,columns);

	//printing the array elements
	printArray(my_array,rows,columns);

	//printing the no.of possible paths from first element to last element
	printf("no.of possible paths are:%d\n",my_array[rows-1][columns-1]);

	return 0;
}

/**getArray function to allocate the memory to array and 
  to generate the array elements*/
int **getArray(int rows,int columns) {

	int **array=(int**)calloc(1,rows*sizeof(int*));
	for(int i=0;i<rows;i++) {
		array[i]=(int*)calloc(1,columns*sizeof(int));
	}
	for(int i=0;i<rows;i++) {
		for(int j=0;j<columns;j++) {
			if(i==0 || j==0) {
				array[i][j]=1;
			}
			else {
				array[i][j]=array[i][j-1]+array[i-1][j];
			}
		}
	}
	return array;

}

//printArray function to display the array elements
void printArray(int **array,int rows,int columns) {

	for(int i=0;i<rows;i++) {
		for(int j=0;j<columns;j++) {
			printf("%d ",array[i][j]);
		}
		printf("\n");
	}

}

/*************************************
OUTPUT:
-------
enter the no.of rows and columns:5 7
1 1 1 1 1 1 1 
1 2 3 4 5 6 7 
1 3 6 10 15 21 28 
1 4 10 20 35 56 84 
1 5 15 35 70 126 210 
no.of possible paths are:210
*************************************/
