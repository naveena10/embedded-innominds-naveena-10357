/***********************************************************************************************
Author:
-------
K.NAVEENA	10357	naveenakolanupaka10@gmail.com	8374415216

Purpose:
--------
	--To perform basic operations like enqueue,dequeue,search and display operations 
	  on circuilar queue

************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#define SIZE 3

//declaring the functions used in this file
struct student *enqueue(struct student *);
void dequeue(struct student **);
void display(struct student *);
int search(struct student *,int);

//defining the structure named as student
struct student {
	int roll;
	char name[20];
	struct student *next;
};

//declaring the global variables
int count;

int main() {

	//declaring the local variables
	int ch,data,flag=0;
	struct student *head=NULL;
	//printing the menu for circuilar queue operations
	while(1) {
		
		printf("-----------MENU----------\n1.enque\n2.display\n3.search\n4.deque\n5.exit\n-----------\n");
		//reading the user's choice
		printf("enter your choice\n");
		scanf("%d",&ch);
		//nevigating according to user's choice
		switch(ch) {
			case 1: 
				head=enqueue(head);
				break;
			case 2:
				display(head);
			       break;
			case 3:
			       {
				printf("enter the roll to search\n");
			       	scanf("%d",&data);
			       	flag=search(head,data);
			       	if(flag==0) {
				       printf("the data is not found\n"); 
				}
			       }
			       break;
			case 4: 
			       	dequeue(&head);
				printf("successfully removed from the queue \n");
				break;
			case 5:
				exit(0);
			       	break;
			default:
				printf("enter your correct choice\n");
		}
	}
	return 0;
}

//defining the enqueue function to add the record into queue
struct student *enqueue(struct student *ptr) {
	struct student *newnode=NULL,*temp=NULL;
	newnode=malloc(sizeof(struct student));
	if(newnode==NULL) {
		printf("Error memory allocation is not done\n");
		return NULL;
	}

	if(count==SIZE) {
		printf("Queue is full\n");
		return ptr;
	}

	printf("enter the roll\n");
	scanf("%d",&newnode->roll);
	printf("enter the name\n");
	scanf("%s",newnode->name);

	if(ptr==NULL) {
		count++;
		ptr=newnode;
		ptr->next = newnode;
	}

	else {
		count++;
		temp=ptr;
		while(temp->next!=ptr) {
			temp=temp->next;
		}
		temp->next=newnode;
		newnode->next = ptr;
	}

	return ptr;
}

//defining the dequeue function to delete the record from the queue
void dequeue(struct student **ptr) {
	struct student *temp=NULL;
	struct student *temp1=NULL;

	if(*ptr==NULL) {
		printf("Queue is empty\n");
	}

	else if(count==1 ) {
		free(*ptr);
		*ptr=NULL;
		count--;
	}
	else {
		temp=*ptr;
		*ptr=(*ptr)->next;
		for(temp1=*ptr;temp1->next!=temp;temp1=temp1->next);
		temp1->next=*ptr;
		free(temp);
		temp=NULL;
		count--;
	}
}

//defining the display function to display all the records present in the queue
void display(struct student *ptr) {
	struct student *temp=NULL;
	temp=ptr;
	if(temp==NULL) {
		printf("Queue is empty\n");
		return;
	}

	printf("-----------------student details are ------------------\n");
	do {
		printf("student id=%d\nname=%s\n",temp->roll,temp->name);
		temp=temp->next;
	printf("-------------------------------------------------------\n");
	} while(temp!=ptr);
}

//defining the search function to search the record in the queue
int search(struct student *ptr, int d) {
	if(ptr==NULL) {
		printf("the queue is empty\n");
		return 0;
	}
	struct student *temp=ptr;
	do {
		if(ptr->roll==d) {
			printf("------------------------------------------\n");
			printf("the found details with that id are\n");
			printf("id=%d\nname=%s\n",ptr->roll,ptr->name);
			printf("------------------------------------------\n");
			return 1;
		}
		ptr=ptr->next;
	} while(ptr!=temp);
	return 0;

}

/**************************************************************

SAMPLE OUTPUT:
--------------
-----------MENU----------
1.enque
2.display
3.search
4.deque
5.exit
-----------
enter your choice
1
enter the roll
1
enter the name
naveena
-----------MENU----------
1.enque
2.display
3.search
4.deque
5.exit
-----------
enter your choice
2
-----------------student details are ------------------
student id=1
name=naveena
-------------------------------------------------------
-----------MENU----------
1.enque
2.display
3.search
4.deque
5.exit
-----------
enter your choice
1
enter the roll
2
enter the name
vidya
-----------MENU----------
1.enque
2.display
3.search
4.deque
5.exit
-----------
enter your choice
2
-----------------student details are ------------------
student id=1
name=naveena
-------------------------------------------------------
student id=2
name=vidya
-------------------------------------------------------
-----------MENU----------
1.enque
2.display
3.search
4.deque
5.exit
-----------
enter your choice
3
enter the roll to search
2
------------------------------------------
the found details with that id are
id=2
name=vidya
------------------------------------------
-----------MENU----------
1.enque
2.display
3.search
4.deque
5.exit
-----------
enter your choice
4
successfully removed from the queue
-----------MENU----------
1.enque
2.display
3.search
4.deque
5.exit
-----------
enter your choice
2
-----------------student details are ------------------
student id=2
name=vidya
-------------------------------------------------------
-----------MENU----------
1.enque
2.display
3.search
4.deque
5.exit
-----------
enter your choice
5

***************************************************************/
