/****************************************************************************************************** 

  Author          
  -------
  K.NAVEENA        10357   naveenakolanupaka@gmail.com     Cell : 8374415216

  Purpose:
  --------
	--To sort the given array elents by using Bubble Sort technique.

*******************************************************************************************************/

#include <iostream>

using namespace std;

// creating class BubbleSort
class BubbleSort {

	// declaring data members
	int *myarray;
	int size;
	//declaring the member functions as public
    public:
	BubbleSort(int size);
	void bubbleSort();
	void printArray();
	void swap(int &a,int &b);
};

// parameterised constructor for BubbleSort class
BubbleSort::BubbleSort(int size) {
	this->size=size;
	myarray= new int[size];
	cout<<"enter the "<<size<<" array elements:"<<endl;
	for(int i=0;i<size;i++)
		cin>>myarray[i];
}

// method to sort the array elements 
void BubbleSort::bubbleSort() {
	for(int i=0;i<size;i++)
		for(int j=0;j<size-1;j++)
			if(myarray[j]>myarray[j+1])
				swap(myarray[j],myarray[j+1]);
}

// method to print the array elements
void BubbleSort::printArray() {
	for(int i=0;i<size;i++)
		cout<<myarray[i]<<" ";
	cout<<endl;
}

// method to swap the two given elements
void BubbleSort::swap(int &data1,int &data2) {
	int temp;
	temp=data1;
	data1=data2;
	data2=temp;
}


int main()
{
	//declaring the local variables
	int size;
	//reading the array size
	cout<<"enter the array size:";
	cin>>size;
	// creating object for BubbleSort class and passing the size
	BubbleSort object(size);
	cout<<"array elements before sorting:"<<endl;
	// calling printArray method 
	object.printArray();
	// calling bubbleSort method 
	object.bubbleSort();
	cout<<"array elements after sorting:"<<endl;
	// calling printArray method
	object.printArray();
	return 0;
}

/********************************************************************************************************
OUTPUT:
-------
enter the array size:5
enter the 5 array elements:
9
2
5
1
7
array elements before sorting:
9 2 5 1 7 
array elements after sorting:
1 2 5 7 9 
**********************************************************************************************************/
