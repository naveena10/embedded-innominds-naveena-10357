/****************************************************************
Author:
-------
K.NAVEENA 10357 naveenakolanupaka10@gmail.com 8374415216

Purpose:
--------
	--To handle the array index out of bound exception
	--Here we have taken all the value as hard-code values
 
****************************************************************/
#include<iostream>
#include<cstdlib>
#include<exception>

using namespace std;

//deriving the ArrayOutOfBound class from exception class
class ArrayOutOfBound: public exception {
	public: ArrayOutOfBound() {
			cout<<"arraay index out of bound exception....!"<<endl;
		}
};

//creating the class array 
class Array {
	private :
		int *array_ptr;
		int size;
	public:
		Array(int *,int);
		int &operator[](int);

		void printArray() ;
};

//overloading the [] operator
int &Array::operator[] (int index) {
	try {
		if (index >= size) {
			throw ArrayOutOfBound();
		}

	}
	catch (ArrayOutOfBound a) {
		cout<<"Array index out of bound handled"<<endl;
	}
	return array_ptr[index];
}

//constructor of array class
Array::Array(int *ptr,int size) {
	this->size=size;
	array_ptr=NULL;
	if(size!=0) {
		array_ptr=new int[size];
		for (int i=0;i<size;i++) {
			array_ptr[i]=ptr[i];
		}
	}
}

//defing member function printArray to print the array elements 
void Array::printArray() {
	for (int i=0;i<size;i++) {
		cout<<array_ptr[i]<<" "<<endl;
	}
}

//main method
int main(void) {
	//creating an array of size five
	int arr[5]={1,2,3,4,5};
	//creating the object for array class
	Array array(arr,5);
	//changing the elements with in the range
	array[3]=8;
	array[4]=9;
	//printing the array elements
	array.printArray();
	//trying to change the index which is out of bound which will cause to array index out of bound exception 
	array[8]=90;
	return 0;
}

/**************************************************************

SAMPLE OUTPUT:
--------------
1 
2 
3 
8 
9 
arraay index out of bound exception....!
Array index out of bound handled

***************************************************************/
