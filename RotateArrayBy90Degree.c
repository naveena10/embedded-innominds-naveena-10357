/******************************************************************************************************
  Author
  -------
  k.NAVEENA 10357 naveenakolanupaka10@gmail.com	Cell : 8374415216

Purpose:
--------

--Rotating the array by 90°
--Here the array size is user's choice. User can give the size at run time as he require.

 ******************************************************************************************************/

#include <stdio.h>
#include<stdlib.h>

int main()
{
	//declaring the local variables
	int rows,columns;

	//reading the rows and columns of an array
	printf("enter the rows and columns:");
	scanf("%d%d",&rows,&columns);

	//declaring the arrays
	int myarray[rows][columns];
	int resultant_array[columns][rows];

	//reading the array elements
	printf("enter the array elements:");
	for(int i=0;i<rows;i++)
		for(int j=0;j<columns;j++)
			scanf("%d",&myarray[i][j]);

	//displaying the array elements before rotating
	printf("the array elements before rotating:\n");
	for(int i=0;i<rows;i++,printf("\n"))
		for(int j=0;j<columns;j++)
			printf("%d ",myarray[i][j]);
	//rotating the array by 90°
	for(int i=0,j=rows-1;i<rows && j>=0;i++,j--)
		for(int k=0;k<columns;k++)
			resultant_array[k][j]=myarray[i][k];

	//displaying the array elements after rotating
	printf("the array elements after rotating:\n");
	for(int i=0;i<columns;i++,printf("\n"))
		for(int j=0;j<rows;j++)
			printf("%d ",resultant_array[i][j]);
	return 0;
}

/*****************************
OUTPUT:
-------
enter the rows and columns:3 4
enter the array elements:1 2 3 4 5 6 7 8 9 4 7 3 
the array elements before rotating:
1 2 3 4 
5 6 7 8 
9 4 7 3 
the array elements after rotating:
9 5 1 
4 6 2 
7 7 3 
3 8 4
 ***********************/
