//including the header files
#include<stdio.h>
#include<stdlib.h>

int main(int argc,char* argv[]) {
	
	//checking whether the command line arguments are sufficient or not
	if(argc<4) {
		printf("insufficient command line arguments\n");
		exit(1);
	}

	//declaring the local variables
	FILE *fp1,*fp2,*fp3;
	char ch1,ch2;

	//opening the first source file
	//if it fails printing the error message
	fp1=fopen(argv[1],"r");
	if(fp1==NULL) {
		printf("can't open file:%s",argv[1]);
		exit(1);
	}

	//opening the second source file
	//if it fails printing the error message
	fp2=fopen(argv[2],"r");
	if(fp2==NULL) {
		printf("can't open file:%s",argv[2]);
		exit(1);
	}

	//opening the destination file
	//if it fails printing the error message
	fp3=fopen(argv[3],"w");
	if(fp3==NULL) {
		printf("can't open file:%s",argv[3]);
		exit(1);
	}

	//checking the whether the files reaches to end or not
	//if not reading the characters into destination file until the new line found
	while(((ch1=fgetc(fp1)))!=255 || ((ch2=fgetc(fp2))!=255)){
		fseek(fp1,-1,1);
		fseek(fp2,-1,1);
		if((ch1=fgetc(fp1))!=255) {
		fseek(fp1,-1,1);
			while((ch1=fgetc(fp1))!='\n') {
				fputc(ch1,fp3);
			}
			fputc('\n',fp3);
		}
		if((ch2=fgetc(fp2))!=255) {
		fseek(fp2,-1,1);
			while((ch2=fgetc(fp2))!='\n') {
				fputc(ch2,fp3);
			}
			fputc('\n',fp3);
		}

	}
	fputc('\0',fp3);

	//closing the file descriptors
	fclose(fp1);
	fclose(fp2);
	fclose(fp3);

	//returning from main
	return 0;

}


