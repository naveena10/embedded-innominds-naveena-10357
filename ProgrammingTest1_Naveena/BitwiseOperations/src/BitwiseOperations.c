//including the header files
#include"Header.h"

int main() {

	//declaring the global variables
	int data,choice;

	//reading the data from the user
	printf("enter the data:");
	scanf("%d",&data);

	while(1) {

		//printing the what are the operations we can perfornm on the given data
		printf("\n\t-----BITWISE-OPERATIONS-----\n");
		printf("\t\t1.set 8th bit\n\t\t2.count set bits\n\t\t3.2's complement\n\t\t4.binary equivalent\n\t\t5.quit\n");
		//reading the user's choice
		printf("\n\t\tenter your choice:");
		scanf("%d",&choice);
		//priting the entered data
		printf("\n\t\t\tentered data is:%d\n",data);
		//navigating to the user's choice
		switch(choice) {
			case 1:

				{
					int resultant_data=set8thBit(data);
					printf("\t\t\tafter setting 8th bit:%d",resultant_data);
				}
				break;
			case 2:
				{
					int count=countSetBits(data);
					printf("\t\t\tnumber of set bits in given data is:%d\n",count);
				}
				break;
			case 3:
				{
					int complement=twosComplement(data);
					printf("\t\t\t2's complement of data is:%d",complement);
				}
				break;

			case 4:
				binaryEquivalent(data);
				break;
			case 5:
				exit(0);
			default:
				printf("\n\t\t\tinvalid choice\n");


		}
	}
	//returning from main
	return 0;
}
