//including the header files
#include"Header.h"

//defining set8thBit function to set the 8th bit int the given data
int set8thBit(int data) {
	data=data|(1<<8);
	return data;
}

//defining the countSetBits functions to count how many set bits are 
//present in the given data
int countSetBits(int data) {
	int temp=data,count=0;
	for(int i=0;i<32;i++) {
		temp=data>>i;
		if(temp && (data>>i)&1)
			count++;
	}
	return count;
}

//defining the binaryEquivalent function to print the binary equivalent of given data
void binaryEquivalent(int data) {
	printf("\t\t\tbinary equivalent is:");
	for(int i=31;i>=0;i--) {
		printf("%d",(data>>i)&1);
	}
	printf("\n");
	return;
}

//defining the twosComplement function to findout the 2's complement of the given data
int twosComplement(int data) {
	data=~data;
	data=data+1;
	return data;
}
