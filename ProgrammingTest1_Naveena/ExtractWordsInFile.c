#include<stdio.h>
#include<string.h>
#include<stdlib.h>

int main(int argc,char* argv[]) {

	if(argc<3) {
		printf("insufficient command line arguments\n");
		printf("please provide source and destination files\n");
		exit(1);
	}

	FILE* fp;
	char* buf,*token;
	int size;
	char delimitor[8]=" .,\n!?\0",s[2]="\n";

	fp=fopen(argv[1],"r");
	if(fp==NULL) {
		printf("can't open file:%s\n",argv[1]);
		exit(1);
	}

	fseek(fp,0,2);
	size=ftell(fp);
	rewind(fp);

	buf=(char*)calloc(1,(size+1)*sizeof(char));

	fread(buf,size,1,fp);
	fclose(fp);

	fp=fopen(argv[2],"w");
	if(fp==NULL) {
		printf("can't open file:%s\n",argv[2]);
		exit(1);
	}

	for(int i=0;i<size;i++) {
		if((buf[i]<='z' && buf[i]>='a') || (buf[i]<='Z'&&buf[i]>='A')) {
			fputc(buf[i],fp);
		}
		else {
			fputc('\n',fp);
		}
	}

	  /*token=strtok(buf,delimitor);
	  while(token!=NULL) {
	  for(int i=1;i<strlen(delimitor);i++) {
	  if(token!=NULL) {
	  strcat(token,s);
	  fprintf(fp,"%s",token);
	  }
	  }
	  token=strtok(NULL,delimitor+i);
	  }*/
	fclose(fp);

	return 0;
}
