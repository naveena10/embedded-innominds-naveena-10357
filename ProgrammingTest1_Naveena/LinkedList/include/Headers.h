#include<stdio.h>
#include<stdlib.h>

#define COUNT 5

typedef struct Student {
	char name[20];
	int roll_no;
	struct Student *next;
}Node;

void insertAtBeginning(Node**);
void displayAllRecords(Node*);
