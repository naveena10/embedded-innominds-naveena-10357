//include header files where the function declarations are present 
#include"Headers.h"

//defining the insertAtBeginning function which insert the given record at the beginning of the list
void insertAtBeginning(Node** head) {
	
	Node *new_record=(Node*)calloc(1,sizeof(Node));
	
	printf("enter the student name:");
	scanf("%s",new_record->name);
	
	printf("enter the student roll number:");
	scanf("%d",&new_record->roll_no);
	
	new_record->next=(*head);
	(*head)=new_record;

	return;
}

//defining the displayAllRecords function which displays the records present in the list
void displayAllRecords(Node* head) {

	printf("\n--------entered %d records details---------\n",COUNT);
	
	for(int i=1;head;head=(head)->next,i++) {
		printf("\tStudent-%d:\n\t\tName:%s\n\t\troll no:%d\n",i,head->name,head->roll_no);
	}

	return;
}
		
