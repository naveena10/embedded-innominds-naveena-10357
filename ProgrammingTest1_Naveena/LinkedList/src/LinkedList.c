//including the header files
#include"Headers.h"

//main function from which the program execution will be start
//and which calls the all other functions needed by this program
int main() {
	
	//creating pointer for the structure named as student which is typedefined as Node
	Node *head=NULL;

	//calling the insertAtBeginning function for 5 times to add the record at beginning of the list
	for(int i=0;i<COUNT;i++) {
		insertAtBeginning(&head);
	}

	//calling the displayAllRecords function to display the records present in the list
	displayAllRecords(head);

	//returning from main
	return 0;
}

