/****************************************************************************************************** 

Author                  
-------
k.NAVEENA    10357   naveenakolanupaka10@gmail.com   Cell : 8374415216

Purpose:
--------
    --To swap the given two numbers

******************************************************************************************************/
#include<stdio.h>

int main() {
	
	//declaring the local variables data1 and data2
	int data1,data2;

	//reading the data from user
	printf("enter data1 and data2:");
	scanf("%d%d",&data1,&data2);
	
	//displaying the numbers before swaping
	printf("before swaping:data1=%d data2=%d\n",data1,data2);

	//swapping the given numbers
	data1^=data2^=data1^=data2;

	//displaying the numbers after swaping
	printf("after swaping:data1=%d data2=%d\n",data1,data2);

	//returning from main
	return 0;
}

/****************************************

OUTPUT:
-------
enter data1 and data2:1 -1
before swaping:data1=1 data2=-1
after swaping:data1=-1 data2=1

****************************************/
