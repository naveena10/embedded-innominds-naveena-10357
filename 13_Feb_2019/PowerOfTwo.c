/****************************************************************************************************** 

Author                  
-------
k.NAVEENA    10357   naveenakolanupaka10@gmail.com   Cell : 8374415216

Purpose:
--------
    --To check whether the given number is power of two or not

******************************************************************************************************/

#include<stdio.h>

int main() {

	//declaring the local variables
	int data,count;

	//reading the data from user
	printf("enter the data:");
	scanf("%d",&data);

	//counting the number of set bits
	for(int i=0;i<=31;i++) {
		if( (data>>i) && ((data>>i)&1) ) 
			count++;
		if(count>1)
			break;
	}

	//displaying the result whether it is power of two or not
	if(count==1) {
		printf("entered number %d is power of 2\n",data);
	}
	else {
		printf("entered number %d is not power of 2\n",data);	
	}

	//returninh from main
	return 0;

}

/**************************************************

OUTPUT1:
-------
enter the data:12
entered number 12 is not power of 2

OUTPUT2:
--------
enter the data:16
entered number 16 is not power of 2

**************************************************/
