/****************************************************************************************************** 

Author                  
-------
k.NAVEENA    10357   naveenakolanupaka10@gmail.com   Cell : 8374415216

Purpose:
--------
    --To manipulate the bits in an integer data like setting the bit,clearing the bit
	toggling the bit,checking the bit,checking the lowest set bit,clearing the bits
	from lsb to given bit position,counting the set bits and printing the binary equivalent
    --Here we are taking the run time inputs for data and bit position

******************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<stdio_ext.h>

//declaring the functions used in this file
void binaryEquivalent(int);
void setNthBit(int,int);
void clearNthBit(int,int);
void toggleNthBit(int,int);
void bitstatus(int,int);
void lowestSetBit(int);
void clearLsbToNthBit(int,int);
void countSetBits(int);

int main() {
	//declaring the local variables
	int data,position;
	char choice;
	//reading the data
	printf("enter the data:");
	scanf("%d",&data);
	while(1) {
		//desplaying the menu of bit-operations
		printf("\n********BIT-WISE operations********");
		printf("\n1.print binary equivalent\n2.set nth bit\n3.clear nth bit\n4.toggle nth bit\n5.bit status\n6.lowest set bit\n7.clear lsb to nth bit\n8.count set bits\n9.quit\n");
		//reading the user's choice
		printf("enter your choice:\n");
		__fpurge(stdin);
		scanf("%c",&choice);
		//navigating according to user's choice
		switch(choice) {	
			case '1':
				printf("\tentered data:%d\n",data);	
				binaryEquivalent(data);
				break;
			case '2':
				printf("\tenter the position of bit which you want to set:");
				scanf("%d",&position);	
				while( position<0 || position>31 ) {
					printf("\tinvalid bit position.Enter valid one(0-31)");
					scanf("%d",&position);
				}
				setNthBit(data,position);
				break;
			case '3':
				printf("\tenter the position of bit which you want to clear:");
				scanf("%d",&position);
				while( position<0 ||position>31 ) {
					printf("\tinvalid bit position.Enter valid one(0-31)");
					scanf("%d",&position);
				}		
				clearNthBit(data,position);
				break;
			case '4':	
				printf("\tenter the position of bit which you want to toggle:");
				scanf("%d",&position);	
				while( position<0 || position>31 ) {
					printf("\tinvalid bit position.Enter valid one(0-31)");
					scanf("%d",&position);
				}
				toggleNthBit(data,position);
				break;
			case '5':	
				printf("\tenter the position of bit which you want to check:");
				scanf("%d",&position);
				while( position<0 || position>31 ) {
					printf("\tinvalid bit position.Enter valid one(0-31)");
					scanf("%d",&position);
				}	
				bitstatus(data,position);
				break;
			case '6':	
				lowestSetBit(data);
				break;
			case '7':	
				printf("\tenter the position of bit upto where you want to clear:");
				scanf("%d",&position);	
				while( position<0 || position>31 ) {
					printf("\tinvalid bit position.Enter valid one(0-31)");
					scanf("%d",&position);
				}
				clearLsbToNthBit(data,position);
				break;
			case '8':	
				countSetBits(data);
				break;
			case '9':	
				exit(0);
			default:
				printf("\tinvalid choice\n");
		
		}
	}
	return 0;
}

//defining binaryEuivalent function which will print the binary equivalent of given number
void binaryEquivalent(int data) {
	printf("\tin binary:");	
	for(int i=31;i>=0;i--)
		printf("%d",(data>>i)&1);
	printf("\n");
	return;

}

//defining the setNthBit method which will set the bit at given position
void setNthBit(int data,int position) {
	printf("\tentered data before setting %d-position bit:%d\n",position,data);
	binaryEquivalent(data);
	data=(data|(1<<position));
	printf("\tentered data after setting %d-position bit:%d\n",position,data);
	binaryEquivalent(data);
	return;
}

//defining the clearNthBit method which will clear the bit at given position
void clearNthBit(int data,int position) {
	printf("\tentered data before clearing %d-position bit:%d\n",position,data);
	binaryEquivalent(data);
	data=(data&(~(1<<position)));
	printf("\tentered data after clearing %d-position bit:%d\n",position,data);
	binaryEquivalent(data);
	return;
}

//defining the toggleNthBit method which will toggle the bit at given position
void toggleNthBit(int data,int position) {
	printf("\tentered data before toggling %d-position bit:%d\n",position,data);
	binaryEquivalent(data);
	data=(data^(1<<position));
	printf("\tentered data after toggling %d-position bit:%d\n",position,data);
	binaryEquivalent(data);
	return;
}

//defining the bitstatus method which will check the bit at given position
void bitstatus(int data,int position) {
	printf("\tentered data is:%d\n",data);
	binaryEquivalent(data);
	printf("\tbit at position %d is:%d\n",position,((data>>position)&1));
	return;
}

//defining the lowestSetBit function which will check which one is the lowest set bit in the given data
void lowestSetBit(int data) {
	printf("\tentered data:%d\n",data);
	binaryEquivalent(data);
	for(int i=0;i<=31;i++) {
		if((data>>i)&1) {
			printf("\tlowest set bit is:%d\n",i);
			return;
		}
	}
	printf("\tthere is no set bit in number:%d\n",data);
	return;
}

//defining the clearLsbNthBit function which will clear the bits from lsb to given bit position
void clearLsbToNthBit(int data,int position) {
	printf("\tentered data before clearing:%d\n",data);
	binaryEquivalent(data);
	for(int i=0;i<=position;i++) {
		data=(data&(~(1<<i)));	
	}
	printf("\tentered data after clearing upto bit position %d is:%d\n",position,data);
	binaryEquivalent(data);
	return;
}

//defining the countSetBits function which will count the set bits present in the given data
void countSetBits(int data) {
	int set_bit_count=0;
	for(int i=0;i<=31;i++) {
		if( (data>>i)&1 )
			set_bit_count++;
	}
	printf("\tno.of set bits in given data %d=%d",data,set_bit_count);
	return;
}

/****************************************************************************
SAMPLE OUTPUT:
--------------
enter the data:10

********BIT-WISE operations********
1.print binary equivalent
2.set nth bit
3.clear nth bit
4.toggle nth bit
5.bit status
6.lowest set bit
7.clear lsb to nth bit
8.count set bits
9.quit
enter your choice:
1
	entered data:10
	in binary:00000000000000000000000000001010

********BIT-WISE operations********
1.print binary equivalent
2.set nth bit
3.clear nth bit
4.toggle nth bit
5.bit status
6.lowest set bit
7.clear lsb to nth bit
8.count set bits
9.quit
enter your choice:
2
	enter the position of bit which you want to set:2
	entered data before setting 2-position bit:10
	in binary:00000000000000000000000000001010
	entered data after setting 2-position bit:14
	in binary:00000000000000000000000000001110

********BIT-WISE operations********
1.print binary equivalent
2.set nth bit
3.clear nth bit
4.toggle nth bit
5.bit status
6.lowest set bit
7.clear lsb to nth bit
8.count set bits
9.quit
enter your choice:
8
	no.of set bits in given data 10=2

********BIT-WISE operations********
1.print binary equivalent
2.set nth bit
3.clear nth bit
4.toggle nth bit
5.bit status
6.lowest set bit
7.clear lsb to nth bit
8.count set bits
9.quit
enter your choice:
9
*********************************************************************/
