/****************************************************************************************************** 

Author                  
-------
k.NAVEENA    10357   naveenakolanupaka10@gmail.com   Cell : 8374415216

Purpose:
--------
    --To find out the range of an signed integer

******************************************************************************************************/

#include<stdio.h>

int main() {
	
	//declaring the local variable data
	int data=1;

	//changing the data to its maximum value
	for(int i=1;i<=30;i++) {

		data=(data<<1)|1;

	}
	
	//displaying the minimum and maximum values of an integer
	printf("range of signed integer is:%d to %d\n",~data,data);
	
	//returning from main
	return 0;

}

/*****************************************************

OUTPUT:
-------
range of signed integer is:-2147483648 to 2147483647

*****************************************************/
