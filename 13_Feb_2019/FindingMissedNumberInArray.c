/******************************************************************
AUTHOR:
-------
K.NAVEENA	10357	naveenakolanupaka10@gmail.com	8374415216

PURPOSE:
--------
	--To find the missing number in the array which has the numbers 
	from given minimum to  maximum values of length (maximum-minimum). 
*******************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>

int main() {
	
	//declaring the local variables
	int size,min,max,i,j,total_sum=0,array_sum=0;
	srand(getpid());
	//reading the minimum and maximum values 
	printf("enter the min and max values:");
	scanf("%d%d",&min,&max);
	size=max-min;
	//declaring the array
	int my_array[size];
	//generating the array elements through rand function
	for(i=0;i<size;i++) {
		my_array[i]=rand()%(size+1)+min;
		for(j=0;j<i;j++) {
			if(my_array[i]!=my_array[j])
				continue;
			else
				break;
		}
		if(i==j) continue;
		else i--;

	}
	//displaying the array elements
	printf("the array elements are:");
	for(i=0;i<size;i++) {
		printf("%d ",my_array[i]);
		array_sum+=my_array[i];
	}
	printf("\n");
	//calculating the total sum
	for(j=min;j<=max;j++) {
		total_sum+=j;
	}
	//printing the missing number in the array
	printf("missing number in the array is:%d\n",total_sum-array_sum);
}

/*****************************************
sAMPLE OUTPUT:
--------------
enter the min and max values:1 10
the array elements are:5 7 8 2 3 1 6 10 9
missing number in the array is:4
*****************************************/
