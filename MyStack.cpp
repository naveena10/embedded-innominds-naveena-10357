/****************************************************************************************************** 
  	Author		
        -------
	K.NAVEENA	10357	naveenakolanupaka10@gmail.com	    Cell : 8374415216

        Purpose:
        --------
		--This program  is to  implement stack,which follows Last in First out approach.
        	--Here we have taken the data member as name.
		--using push,pop member functions we are entering the data onto stack and deleting the data from the stack 
******************************************************************************************************/
	
#include<iostream>
#include<stdio.h>
#include<stdlib.h>

using namespace std;

// creating class mystack
class MyStack {
	// declaring the data members 
	struct stack {
		string name;		
		struct stack *next;
	}*top;
	//declaring the member functions as public
   public:
	MyStack();
	void push(string name);
	string pop();
	void displayRecords();
	~MyStack();
};
	// default constructor for MyStack class
	MyStack::MyStack() {
		top=NULL;
	}
	// method to push the data into stack
	void MyStack::push(string name) {
		struct stack *new_record=new struct stack;
		new_record->name=name;
		if(top==NULL) {
			top=new_record;
			new_record->next=NULL;
			return;
		}
		new_record->next=top;
		top=new_record;
		return;
	}
	
	// method to pop the data from the stack	
	string MyStack::pop() {
		string data;
		if(top==NULL) {
			cout<<"stack underflow"<<endl;
			return NULL;
		}
		struct stack *temp=top;
		data=temp->name;
		top=top->next;
		delete temp;
		temp=NULL;
		return data;
	}
	
	// method to display the data present in the stack
	void MyStack::displayRecords() {
		struct stack *temp=top;
		cout<<"\t*****records in stack:*****"<<endl;
		for(;temp!=NULL;temp=temp->next) {
			cout<<"\t\t"<<temp->name<<endl;
		}
	}
	// destructor for MyStack class 
	MyStack::~MyStack() {
                delete top;
        }

//main method 
int main(void) {
	// creating object  for mystack class
	MyStack mystack;
	//declaring the local variables	
	int choice;
	string name;

	while(1) {
		cout<<"*****MENU*****"<<endl;
		cout<<"1.push\n2.pop\n3.display records\n4.quit"<<endl;
		cout<<"enter your choice:"<<endl;
		cin>>choice;
		switch(choice) {
			case 1:
				cout<<"enter name:"<<endl;
				cin>>name;
				// calling push method to enter the data onto stack
				mystack.push(name);
				break;
			case 2:
				// calling pop method to delete the data from stack
				cout<<"record with data:"<<mystack.pop()<<" was deleted"<<endl;
				break;
			case 3:
				// calling display method to print the data present in the stack
				mystack.displayRecords();
				break;
			case 4:
				exit(0);
			default:
				cout<<"invalid choice"<<endl;
		}
	} 

 

}
/*****************************************************************************
OUTPUT:
-------
  *****MENU*****
1.push
2.pop
3.display records
4.quit
enter your choice:
1
enter name:
naveena
*****MENU*****
1.push
2.pop
3.display records
4.quit
enter your choice:
1
enter name:
nandini
*****MENU*****
1.push
2.pop
3.display records
4.quit
enter your choice:
3
	*****records in stack:*****
		nandini
		naveena
*****MENU*****
1.push
2.pop
3.display records
4.quit
enter your choice:
2
record with data:nandini was deleted
*****MENU*****
1.push
2.pop
3.display records
4.quit
enter your choice:
3
	*****records in stack:*****
		naveena
*****MENU*****
1.push
2.pop
3.display records
4.quit
enter your choice:
4
******************************************************************************/




