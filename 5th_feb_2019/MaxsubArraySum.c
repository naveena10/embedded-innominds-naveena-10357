/****************************************************************************************************** 
  	Author			
        -------
	k.NAVEENA	10357	naveenakolanupaka10@gmail.com	Cell : 8374415216

        Purpose:
        --------

        --To find out maximum sum of the sub array in the given array

******************************************************************************************************/
//including hte header files
#include<stdio.h>
#include<stdlib.h>

//declaring the functions used in this file
int max(int first,int second);
int maxSubArraySum(int array[],int size);

int main() {

	//declaring the local variables
	int size,max_sum;
	//reading the array size from user
	printf("enter the size:");
	scanf("%d",&size);
	//declaring the array with given size
	int array[size];
	//reading the array elements
	printf("enter the array elements:");
	for(int i=0;i<size;i++) {
		scanf("%d",&array[i]);
	}
	//priting the array elements
	printf("the array elements are:");
	for(int i=0;i<size;i++) {
		printf("%d ",array[i]);
	}
	printf("\n");
	//calling the maxSubArraySum function which returns the maximum sum of the sub array 
	max_sum=maxSubArraySum(array,size);
	//printing the maximum sum
	printf("max sub array sum is:%d\n",max_sum);
	//returning from main
	return 0;
}

//defining the max function which will return the maximum number of two given numbers
int max(int first,int second) {
	if(first>second) return first;
	else return second;
}

//defining the maxSubArraySum function which will return the maximum sum of the sub array
int maxSubArraySum(int array[],int size) {
	int max_so_far=array[0];
	int cur_max=0;

	for(int i=0;i<size;i++) {
		cur_max=max(array[i],cur_max+array[i]);
		max_so_far=max(max_so_far,cur_max);
	}
	return max_so_far;
}

/****************************************************************************
SAMPLE OUTPUT:
------------    
	enter the size:10
	enter the array elements:1 2 -3 4 5 7 -4 3 -2 -3
	the array elements are:1 2 -3 4 5 7 -4 3 -2 -3 
	max sub array sum is:16
*****************************************************************************/
