#include <stdio.h>
#include <stdlib.h>

//defining the structure
struct Employee {

char employee_name[10];
int employee_id;
struct Employee *next;
};

//declaring the functions used in the main source code
void addAtBeginning(struct Employee **); 
void addAtEnding(struct Employee **); 
void addAtMiddle(struct Employee **); 
void displayAllRecords(struct Employee *); 
void deleteAtBeginning(struct Employee **); 
void deleteAtEnding(struct Employee **); 
void deleteAtMiddle(struct Employee **); 
int updateRecord(struct Employee **,int); 
