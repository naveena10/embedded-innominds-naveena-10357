#include<Headers.h>

//defing the addAtBeginning function to add the record at beginning of the list
void addAtBeginning(struct Employee **head) {
    struct Employee *new_record=calloc(1,sizeof(struct Employee));
    printf("enter the employee name:");
    scanf("%s",new_record->employee_name);
    printf("enter employee id:");
    scanf("%d",&new_record->employee_id);

    if(*head == NULL) {

       *head = new_record;
        return;

    }

    new_record->next = *head;
    *head = new_record;
     return;
}

//defing the addAtEnding function to add the record at ending of the list
void addAtEnding(struct Employee **head) {
    struct Employee *new_record=calloc(1,sizeof(struct Employee));
    printf("enter the employee name:");
    scanf("%s",new_record->employee_name);
    printf("enter employee id:");
    scanf("%d",&new_record->employee_id);
    if(*head == NULL) {

       *head = new_record;
        return;

    }
    struct Employee *temp;
    for(temp=*head;temp->next!=NULL;temp=temp->next);
    temp->next=new_record;
    return;
}

//defing addAtMiddle function to add the record at middle of the list
void addAtMiddle(struct Employee **head) {

    struct Employee *new_record=calloc(1,sizeof(struct Employee));
    printf("enter the employee name:");
    scanf("%s",new_record->employee_name);
    printf("enter employee id:");
    scanf("%d",&new_record->employee_id);

    if(*head == NULL) {
         printf("list is empty so directly adding the record to list\n");
       *head = new_record;
        return;
    }

    if((*head)->next == NULL) {
       int choice;
         printf("only one record is present in the list. Do you want to add at 1.first or 2.last?\nenter your choice:");
         scanf("%d",&choice);
         if(choice==1) {
           new_record->next = *head;
             *head = new_record;
             return;
         }
         else if(choice==2) {
           (*head)->next=new_record;
           return;
         }
         else {
         printf("invalid choice. exiting...");
         return;
    }
    }
     if((*head)->next->next==NULL) {
     new_record->next=(*head)->next;
     (*head)->next=new_record;
     return;
    }
     struct Employee *fast,*slow;
     for(fast=*head,slow=*head;fast->next!=NULL && fast->next->next!=NULL;fast=fast->next->next,slow=slow->next);
     new_record->next=slow->next;
     slow->next=new_record;
     return;
}

//defing the displayAllRecords function to display the all records present in the list
void displayAllRecords(struct Employee *head) {

     if(head==NULL) {
         printf("list is empty\n");
         return;
      }
     struct Employee *temp;
     for(temp=head;temp!=NULL;temp=temp->next)
     printf("%s %d\n",temp->employee_name,temp->employee_id);

}

//defing the deleteAtBeginning function to delete the record present at beginning
void deleteAtBeginning(struct Employee **head) {

      if(*head==NULL) {
         printf("list is empty\n");
         return;
      }
      struct Employee *temp=*head;
      *head=(*head)->next;
      free(temp);
      temp=NULL;
      return;
}

//defing the deleteAtEnding function to delete the record present at end of the list 
void deleteAtEnding(struct Employee **head) {

      if(*head==NULL) {
         printf("list is empty\n");
         return;
      }
      struct Employee *temp,*previous;
      for(temp=*head;temp->next!=NULL; previous=temp,temp=temp->next);
      previous->next=temp->next;
      free(temp);
      temp=NULL;
      return;
}

//defing the deleteAtMiddle function to delete the record present at middle in the list
void deleteAtMiddle(struct Employee **head) {

struct Employee *temp=*head;

if(*head == NULL) {
         printf("list is empty\n");
        return;
    }

    if((*head)->next == NULL) {
         printf("only one record is present in the list.So directly deleting the record from list\n");
           *head=NULL;
           free(temp);
           temp=NULL;
         return;
    }
     if((*head)->next->next==NULL) {
       int choice;
     printf("only two records are present in list. want to delete 1.first or 2.last?\nenter your choice:");
     scanf("%d",&choice);
     if(choice==1) {
           deleteAtBeginning(head);
         }
                                                
         else if(choice==2) {
           deleteAtEnding(head);
         }
         else {
         printf("invalid choice. exiting...");
     }
     return;
     }
     struct Employee *fast,*slow;
     for(fast=*head,slow=*head;fast->next!=NULL && fast->next->next!=NULL;fast=fast->next->next,temp=slow,slow=slow->next);
     temp->next=slow->next;
     free(slow);
     slow=NULL;
     return;

}

//defing the updateRecord function to update the existing record in the list
int updateRecord(struct Employee **head,int id) {

int flag=0,choice;
struct Employee *temp=NULL;
      for(temp=*head;temp!=NULL;temp=temp->next) {

        if(temp->employee_id==id) {
              printf("entered id is found and the details are...\n");
              printf("%s %d\n",temp->employee_name,temp->employee_id);
   printf("which one do you want to update? 1.name 2.id\nenter your choice:");
   scanf("%d",&choice);
   if(choice==1) {
           flag++;
           printf("enter the new name to be updated:");
           scanf("%s",temp->employee_name);
         }
         else if(choice==2) {
           flag++;
           printf("enter the id to be updated:");
           scanf("%d",&temp->employee_id);
         }
         else {
         printf("invalid choice. exiting...");
     }
     }
     }
     return flag;
}

