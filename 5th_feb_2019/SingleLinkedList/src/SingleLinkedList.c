#include"Headers.h"

int main()
{
	//declaring the local variables
	struct Employee *head=NULL;
	int choice;
	//printing the menu of SingleLinkedList operations
	while(1) {
		printf("\n******OPERATIONS ON SINGLE LINKED-LIST******\n");
		printf("1.add at beginning\n2.add at ending\n3.add at middle\n4.update the record\n5.delete at beginning\n6.delete at ending\n7.delete at middle\n8.display all records\n9.quit\n\n");
		//reading the user choice
		printf("enter your choice");
		scanf("%d",&choice);
		//navigating according to user's choice
		switch(choice) {
			case 1:
				addAtBeginning(&head);
				break;
			case 2:
				addAtEnding(&head);
				break;

			case 3:
				addAtMiddle(&head);
				break;
			case 4:
				{
					int id;
					printf("enter the id to be update:");
					scanf("%d",&id);
					int flag=updateRecord(&head,id);
					if(flag) printf("successfully updated the record");
					else printf("entered id is not present in the list\n");

				}
				break;
			case 5:
				deleteAtBeginning(&head);
				break;
			case 6:
				deleteAtEnding(&head);
				break;
			case 7:
				deleteAtMiddle(&head);
				break;
			case 8:
				displayAllRecords(head);
				break;
			case 9:
				exit(0);
			default:
				printf("invalid choice\n");
		}
	}
}
