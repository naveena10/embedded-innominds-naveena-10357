/****************************************************************************************************** 
Author:
-------
	k.NAVEENA 10357 naveenakolanupaka10@gmail.com	Cell:8374415216


Purpose:
--------
	--To multiply the two given arrays if the multiplication is possible

******************************************************************************************************/


#include <stdio.h>

int main()
{
	//declaring the local variables
	int array1_rows,array1_columns,array2_rows,array2_columns;

	//reading the first array size
	printf("enter first array rows and columns:"); scanf("%d%d",&array1_rows,&array1_columns);
	//reading the second array size
	printf("enter second array rows and columns:");     scanf("%d%d",&array2_rows,&array2_columns);
	//checking if the multiplication is possible or not and multiplying if possible
	if(array1_columns == array2_rows) {
		//declaring the arrays
		int array1[array1_rows][array1_columns],array2[array2_rows][array2_columns],resultant_array[array1_rows][array2_columns],i,j;
		//reading the first array elements
		printf("enter the first array elements:");
		for(i=0;i<array1_rows;i++) {
			for(j=0;j<array1_columns;j++) {
				scanf("%d",&array1[i][j]);
			}
		}
		//reading the second array elements
		printf("enter the second array elements:");
		for(i=0;i<array2_rows;i++) {
			for(j=0;j<array2_columns;j++) {
				scanf("%d",&array2[i][j]);
			}
		}
		//multiplying the frist and second arrays and storing the result in another array named as resultant_array
		for(i=0;i<array1_rows;i++) {
			for(j=0;j<array2_columns;j++) {
				resultant_array[i][j]=0;
				for(int k=0;k<array1_columns;k++) {
					resultant_array[i][j]+=array1[i][k]*array2[k][j];
				}
			}
		}
		//displaying the resultant array
		printf("after multiplication the resultant array is:\n");
		for(i=0;i<array1_rows;i++,printf("\n")) {
			for(j=0;j<array2_columns;j++) {
				printf("%d ",resultant_array[i][j]);
			}
		}
	}
	//if multiplication is not possible then printing the message
	else {
		printf("can not multiply the given two arrays\n");
	}
	return 0;
}

/*********************************************

OUTPUT:
-------
enter first array rows and columns:2 3
enter second array rows and columns:3 2
enter the first array elements:1 2 3 4 5 6 
enter the second array elements:1 2 3 4 5 6
after multiplication the resultant array is:
22 28 
49 64 

**********************************************/
