/****************************************************************************************************** 
  	Author			
        -------
	k.NAVEENA	10357	naveenakolanupaka10@gmail.com	Cell : 8374415216

        Purpose:
        --------

        --To find out the indexes of two elements present in the sorted-array whose sum is equal to 
	  the given number

******************************************************************************************************/
	
#include<stdio.h>
#include<stdlib.h>

//declaring the functions used in the file
int *getArray(int);
int* findSumPair(int*,int,int);

//main function which calls the all other functions
int main() {
	
	//declaring the the local variables
	int size,number,*my_array,*index_array;

	//reading the array size
	printf("enter the size of the array:");
	scanf("%d",&size);

	//reading the number to be find out in the array
	printf("enter the number to be find out:");
	scanf("%d",&number);

	//calling the getArray function to genereate the array
	my_array=getArray(size);

	//calling the findSumPair function to find the given number
	index_array=findSumPair(my_array,number,size);

	//printing the indexes where the given element is found
	if(index_array[0] == -1 && index_array[1] == -1) {
		printf("given number %d sum is not found in the array\n");
	}
	else {
		printf("pair of indexes are:%d&%d\n",index_array[0],index_array[1]);
	}

	return 0;
}

//getArray function to get the array elements
int *getArray(int size) {

	int *array=(int*)malloc(size*sizeof(int));
	printf("enter the array elements in sorted way:");
	for(int i=0;i<size;i++)
		scanf("%d",&array[i]);
	return array;

}

//findSumPair function to find out the indexes whose elements sum is equal to the given number 
int *findSumPair(int *array,int number,int size) {

	int first_number,second_number,sum,*forward_ptr,*backward_ptr;
	int *temp=(int*)malloc(2*sizeof(int));
	temp[0]=-1;
	temp[1]=-1;
	forward_ptr=array+0;
	backward_ptr=array+(size-1);
	while(forward_ptr<backward_ptr) {
		first_number=*forward_ptr;
		second_number=*backward_ptr;
		sum=first_number+second_number;
		if(sum<number) 
			forward_ptr++;
		else if(sum>number) 
			backward_ptr--;
		else {
			temp[0]=forward_ptr-array;
			temp[1]=backward_ptr-array; 
			break;
		}
	}
	return temp;

}

/*********************************
OUTPUT:
-------
enter the size of the array:5
enter the array elements:1 2 3 4 5 
enter the number to be find out:7
pair of indexes are:1&4
**********************************/
