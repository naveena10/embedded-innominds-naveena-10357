/****************************************************************************************************** 
  Author			
  -------
	k.NAVEENA	10357	naveenakolanupaka10@gmail.com	Cell : 8374415216

  Purpose:
  --------
     --To find out the febonacci series of the given number using recursion
******************************************************************************************************/

//including th header files
#include<stdio.h>

//declaring the function fibonacciSeries used in this file
int fibonacciSeries(int);

//defining the main function
//which is used to print the febonacci series of the given number
int main() {

	//declaring the local variables 
	int number;
	//reading the data from user
	printf("enter the number:");
	scanf("%d",&number);
	//printing the febonacci series by calling febonacciSeries function
	for(int i=0;i<=number;i++)
		printf("%d ",fibonacciSeries(i));
	printf("\n");
	//returning from main
	return 0;
}

//dfining the febonacciSeries function which will calculate the 
//febonacci number of a given number
int fibonacciSeries(int number) {
	if(number<=0) return 0;
	if(number==1) return 1;
	return fibonacciSeries(number-1)+fibonacciSeries(number-2);
}

/*******************************************************************
SAMPLE OUTPUT:
--------------
enter the number:10
0 1 1 2 3 5 8 13 21 34 55 
*******************************************************************/
