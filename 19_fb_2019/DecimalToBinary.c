/****************************************************************************************************** 
  Author			
  -------
	k.NAVEENA	10357	naveenakolanupaka10@gmail.com	Cell : 8374415216

  Purpose:
  --------
     --To find out the binary equivalent of the given positive number using recursion
******************************************************************************************************/

//including the header files
#include<stdio.h>

//declaring the function decimalToBinary which is used in this file
void decimalToBinary(int);

//defining the main from which the execution of this program will start
int main() {

	//declaring the local variables
	int number;
	//reading the data from user
	printf("enter the number:");
	scanf("%d",&number);
	//clling the function decimalToBinary function
	decimalToBinary(number);
	printf("\n");
	//returning from main
	return 0;
}

//defining the decimalToBinary function 
//which will print the binary equivalent of the given positive number
void decimalToBinary(int n) {
	if(n>0) {
		decimalToBinary(n/2);
		printf("%d",n%2);
	}
	return;
}

/**************************************************************
SAMPLE OUTPUT:
--------------
enter the number:10
1010
**************************************************************/
