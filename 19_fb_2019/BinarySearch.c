/****************************************************************************************************** 
  Author			
  -------
	k.NAVEENA	10357	naveenakolanupaka10@gmail.com	Cell : 8374415216

  Purpose:
  --------
      --To find out where the given number is present in the array through binary-search using recursion
	Here we are generating the array elements randomly depending upon the user's choice 

******************************************************************************************************/

//including hte required headr files
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

//declaring the functions used in this file
void sortArray(int[],int);
int binarySearch(int[],int,int,int);

//defining the main from which it,s excution will be start
int main() {

	//declaring the local variables
	int size,min,max,i,j,k,number,flag;
	srand(getpid());
	//reading the array size from the usr
	printf("enter the size of the array:");
	scanf("%d",&size);
	//checking whether the size is valid or not
	//if not re-reading the size
	while(size<=0) {
		printf("size should be greater than zero\n");
		printf("re-enter the size:");
		scanf("%d",&size);
	}

	//declaring the array
	int my_array[size];
	//rading the range of the array values to generate randomly 
	printf("enter the range(min-max) of the array:");
	scanf("%d%d",&min,&max);
	//checking whether the range is valid or not
	//if not reading it again and again until it valid
	while(!((min<max) && ((max-min)>=size))) {
		printf("enter the proper range:");
		scanf("%d%d",&min,&max);
	}
	//generating the array elements randomly and avoiding the repeatations
	for(i=0;i<size;i++) {
		k=rand()%(max-min)+min;
		for(j=0;j<i;j++) {
			if(my_array[j]==k)
				break;
		}
		if(j==i) {
			my_array[i]=k;
		} else {
			i--;
		}
	}
	//displaying the generated array values
	printf("\tthe array elements before sorting are:");
	for(i=0;i<size;i++) {
		printf("%d ",my_array[i]);
	}
	printf("\n");

	//calling the sortArray function to sort the array elements
	sortArray(my_array,size);
	//displaying the sorted array values
	printf("\tthe array elements after sorting are:");
	for(i=0;i<size;i++) {
		printf("%d ",my_array[i]);
	}
	printf("\n");
	//rading the number to be searched from user
	printf("enter the number to be search:");
	scanf("%d",&number);
	//displaying the position of the number if it is present
	//otherwise printing the proper message
	flag=binarySearch(my_array,0,size-1,number);
	if(flag>=0) {
		printf("\tentered number %d is found at position %d\n",number,flag);
	}
	else {
		printf("\tentered number is not found\n");
	}
	return 0;
}

//defining the sortArray function which will sorts the 
//elements of the given array
void sortArray(int array[],int size) {
	for(int i=0;i<size;i++) {
		for(int j=0;j<size-i-1;j++) {
			if(array[j]>array[j+1]) {
				int temp=array[j];
				array[j]=array[j+1];
				array[j+1]=temp;
			}
		}
	}
}

//defining the binarySearch function which will search the given number in the given array
int binarySearch(int array[],int low,int high,int number) {
	
	if(low<=high) {
		int mid=(low+high)/2;
		if(array[mid]<number) 
			binarySearch(array,mid+1,high,number);
		else if(array[mid]>number) 
			binarySearch(array,0,mid-1,number);
		else 
			return mid;
	}
	else 
		return -1;

}

/********************************************************************************
SAMPLE OUTPUT:
--------------
enter the size of the array:-4
size should be greater than zero
re-enter the size:5
enter the range(min-max) of the array:1
2
enter the proper range:1
3
enter the proper range:1
5
enter the proper range:1
6
	the array elements before sorting are:5 1 4 2 3 
	the array elements after sorting are:1 2 3 4 5 
enter the number to be search:3
	entered number 3 is found at position 2
********************************************************************************/
