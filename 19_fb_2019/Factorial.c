/****************************************************************************************************** 
  Author			
  -------
	k.NAVEENA	10357	naveenakolanupaka10@gmail.com	Cell : 8374415216

  Purpose:
  --------
     --To find out the factorial of a given number using recursion
******************************************************************************************************/

//including the header files 
#include<stdio.h>

//declaring the function factorial used in this file
int factorial(int);

//defining the main function from which the execution of this program will start
int main() {

	//declaring the local variables
	int number;
	//reading the data from user
	printf("enter the number:");
	scanf("%d",&number);
	//displaying the factorial of a given number by calling the factorial function
	printf("factorial of given number %d is %d\n",number,factorial(number));
	//returning from main
	return 0;

}

//defining the factorial function which will calculate the 
//factorial of a given number
int factorial(int number) {

	if(number<=1) return 1;
	return number*factorial(number-1);
}

/**********************************************************************
SAMPLE OUTPUT:
--------------
enter the number:5
factorial of given number 5 is 120
**********************************************************************/
